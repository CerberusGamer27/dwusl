<?php
require_once '../model/Producto.php';
require_once '../model/DaoProducto.php';  //nuevo

$respuesta= null;
$data = array();
$dao = new DaoProducto();              //nuevo

if($_POST){
    if(isset($_POST["key"])){
        $key = $_POST["key"];
        $prod = new Producto();
        switch ($key){
            case 'get':
                $respuesta = $dao->get();
                break;

            case 'getProveedores':
                $respuesta = $dao->getProveedores();
                break;

            case 'fillData':
                $codigo = $_POST["codigo"];
                $respuesta = $dao->fillData($codigo);
                break;

            case 'insertar':
                //pendiente de programar
                parse_str($_POST["data"],$data);
                $prod->setNombre($data["txtNombre"]);
                $prod->setPrecioCompra($data["txtPrecioCompra"]);
                $prod->setPrecioVenta($data["txtPrecioVenta"]);
                $prod->setStock($data["txtStock"]);
                $prod->setStockMinimo($data["txtStockMinimo"]);
                $prod->setIdProveedor($data["selectProveedor"]);
                $prod->setEstado("1");
                $respuesta = $dao->insertar($prod);
                break;
            case 'modificar':
                parse_str($_POST["data"],$data);
                $prod->setCodigo($data["txtCodigo"]);
                $prod->setNombre($data["txtNombre"]);
                $prod->setPrecioCompra($data["txtPrecioCompra"]);
                $prod->setPrecioVenta($data["txtPrecioVenta"]);
                $prod->setStock($data["txtStock"]);
                $prod->setStockMinimo($data["txtStockMinimo"]);
                $prod->setIdProveedor($data["selectProveedor"]);
                if(isset($data["rEstado"])){
                    $prod->setEstado("1");
                } else {
                    $prod->setEstado("0");
                }
                $respuesta = $dao->modificar($prod);
                break;

            case 'eliminar':
                $codigo = $_POST["codigo"];
                $respuesta= $dao->eliminar($codigo);
                break;
        }
    }
}
echo $respuesta;
?>
