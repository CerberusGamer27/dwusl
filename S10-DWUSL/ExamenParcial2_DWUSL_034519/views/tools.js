function cargarTabla() {
    var div = $("#contenido");
    $.ajax({
        url: '../controller/ProductoController.php',
        type: 'post',
        data: {key: 'get'}
    }).done(function (resp) {
        div.empty();
        div.append(resp);
        //para formato de la tabla
        $("#tabla").DataTable({
            searching: true,
            language: {
                "info": "Mostrando _START_ a _END_ de _TOTAL_ Registros",
                "lengthMenu": "Mostrar _MENU_ registros",
                "Search": "Buscar",
                "paginate": {
                    "first": "Primero",
                    "last": "Última",
                    "next": "Siguiente",
                    "previous": "Anterior"
                }
            },
            "order": [], //Initial no order.
        });

    }).fail(function () {
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Error al Cargar la Tabla',
        })
    });
}

async function cargarSelect() {
    var select = $("#selectProveedor");
    await $.ajax({
        url: '../controller/ProductoController.php',
        type: 'post',
        data: {key: 'getProveedores'}
    }).done(function (resp) {
        select.empty();
        select.append(resp);

    }).fail(function () {
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Error al Cargar el Select',
        })
    });
}

function eliminar(codigo) {
    Swal.fire({
        title: '¿Esta seguro que desea eliminar el producto?',
        showDenyButton: true,
        confirmButtonText: 'Eliminar',
        denyButtonText: `Cancelar`,
        icon: 'question'
    }).then((result) => {
        if (result.isConfirmed) {
            //codigo
            $.ajax({
                url: '../controller/ProductoController.php',
                type: 'post',
                data: {key: 'eliminar', codigo: codigo}
            }).done(function (resp) {
                if (resp) {
                    Swal.fire({
                        icon: 'success',
                        title: 'Eliminado Correctamente',
                        showConfirmButton: false,
                        timer: 1500
                    })
                    cargarTabla();
                } else {
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'Error al Eliminar',
                    })
                }
            }).fail(function () {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Error al Eliminar',
                })
            });
        } else if (result.isDenied) {
            Swal.fire('Producto no Eliminado', '', 'info')
        }
    })
}

async function editar(codigo) {
    await cargarSelect();
    $('#exampleModal').modal('show');
    $("#btnGuardar").hide();
    $("#divSwitch").show();
    $("#txtCodigo").prop('disabled', false);
    $("#valorEstado").show();
    $("#btnModificar").show();
    await $.ajax({
        url: '../controller/ProductoController.php',
        type: 'post',
        data: {key: 'fillData', codigo: codigo}
    }).done(function (resp) {
        if (resp) {
            console.log(resp[0]);
            $("#txtCodigo").val(resp[0].codigo).prop('readonly', true);
            $("#txtNombre").val(resp[0].nombre);
            $("#txtPrecioCompra").val(resp[0].precioCompra);
            $("#txtPrecioVenta").val(resp[0].precioVenta);
            $("#txtStock").val(resp[0].stock);
            $("#txtStockMinimo").val(resp[0].stock_minimo);
            $("#selectProveedor").val(resp[0].proveedor);
            if (resp[0].estado === "1") {
                $("#rEstado").attr('checked', true);
            } else {
                $("#rEstado").attr('checked', false);
            }
        }
    }).fail(function () {
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Error al Obtener la informacion',
        })
    });
}

const guardar = (formulario) => {
    $.ajax({
        url: '../controller/ProductoController.php',
        type: 'post',
        data: {key: 'insertar', data: formulario}
    }).done(function (resp) {
        if (resp) {
            Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'Guardado con Exito',
                showConfirmButton: false,
                timer: 1500
            })
            cargarTabla();
            $("#btnCerrarModal").click();
        }
    }).fail(function () {
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Error al Insertar',
        })
    });
}

const modificar = (formulario) => {
    $.ajax({
        url: '../controller/ProductoController.php',
        type: 'post',
        data: {key: 'modificar', data: formulario}
    }).done(function (resp) {
        if (resp) {
            Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'Guardado con Exito',
                showConfirmButton: false,
                timer: 1500
            })
            cargarTabla();
            $("#btnCerrarModal").click();
        }
    }).fail(function () {
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Error al Insertar',
        })
    });
}

function validacionStock (tStock, tStockMinimo) {
    let valorMinimo = parseInt($(tStockMinimo).val());
    let stock = parseInt($(tStock).val());
    let estado;
    if(!isNaN(stock)){
        if(valorMinimo > stock){
            $(tStockMinimo).addClass("is-invalid");
            estado = false;
        } else {
            $(tStockMinimo).removeClass("is-invalid");
            estado = true;
        }
    }
    return estado;
}

function validacionPrecios (tCompra, tVenta){
    let precioVenta = parseInt($(tVenta).val());
    let precioCompra = parseInt($(tCompra).val());
    let estado;
    if(!isNaN(precioCompra)){
        if(precioVenta < precioCompra){
            $(tVenta).addClass("is-invalid");
            estado = false;
        } else {
            $(tVenta).removeClass("is-invalid");
            estado = true;
        }
    }
    return estado;
}



$(document).ready(function () {
    //cargar tabla html
    cargarTabla();

    $('#rEstado').on('change', function () {
        if ($(this).is(':checked')) {
            $('#valorEstado').text('El producto esta activo');
        } else {
            $('#valorEstado').text('El producto esta inactivo');
        }
    });
    //cargar select del modal
    $("#btnNuevo").on("click", function () {
        cargarSelect();
        //$("#txtCodigo").prop('readonly', false);
        $("#btnGuardar").show();
        $("#btnModificar").hide();
        $("#divSwitch").hide();
        $("#valorEstado").hide();
        $("#txtCodigo").prop('disabled', true);
        $("#form1")[0].reset();  //limpiar el form
    })

    $("#txtStockMinimo").on("input", function() {
        validacionStock("#txtStock", "#txtStockMinimo")
    });

    $("#txtPrecioVenta").on("input", function() {
        validacionPrecios("#txtPrecioCompra", "#txtPrecioVenta");
    });

    // Boton Modificar del Modal
    $("#form1").on("submit", function (e) {
        e.preventDefault();
        var formulario = $("#form1").serialize();
        if(formulario.includes("txtCodigo")){
            if(validacionStock("#txtStock", "#txtStockMinimo") && validacionPrecios("#txtPrecioCompra", "#txtPrecioVenta")) {
                modificar(formulario);
            } else {
                Swal.fire({
                    icon: 'error',
                    title: 'Verifique el Formulario',
                    text: 'Necesita arreglar los errores antes de poder guardar cambios'
                })
            }
        } else {
            if(validacionStock("#txtStock", "#txtStockMinimo") && validacionPrecios("#txtPrecioCompra", "#txtPrecioVenta")) {
                guardar(formulario);
            } else {
                Swal.fire({
                    icon: 'error',
                    title: 'Verifique el Formulario',
                    text: 'Necesita arreglar los errores antes de poder guardar cambios'
                })
            }
        }
    })


});