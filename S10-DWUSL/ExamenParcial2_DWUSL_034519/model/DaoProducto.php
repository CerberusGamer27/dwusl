<?php
require_once 'config.php';
require_once 'Producto.php';

class DaoProducto
{
    private $con;

    private function conectar()
    {
        try {
            $this->con = new mysqli(SERVER,
                USER,
                PASS,
                DB);
        } catch (Exception $ex) {
            http_response_code(400);
            return $ex->getMessage();
        }
    }

    private function desconectar()
    {
        $this->con->close();
    }

    public function get()
    {
        //EJEMPLO DE CONSULTAS NO PREPARADAS
        $sql = "select a.codigo, a.nombre, a.precioCompra, a.precioVenta, a.stock, a.stock_minimo, b.nombre as proveedor, a.estado from producto a ";
        $sql .= "inner join proveedor b on a.id_proveedor = b.codigo";
        try {
            $this->conectar();
            $html = "<table class='table table-hover table-sm' id='tabla'><thead><th>CODIGO</th><th>NOMBRE</th><th>PRECIO COMPRA</th><th>PRECIO VENTA</th><th>STOCK</th><th>STOCK MINIMO</th><th>PROVEEDOR</th><th>ESTADO</th><th>ACCIONES</th></thead><tbody>";
            $res = $this->con->query($sql);
            while ($fila = mysqli_fetch_assoc($res)) {
                $estado = ($fila["estado"] == 1) ? "Activo" : "Inactivo";
                $html .= "<tr>";
                $html .= "<td>" . $fila["codigo"] . "</td>";
                $html .= "<td>" . $fila["nombre"] . "</td>";
                $html .= "<td>" . $fila["precioCompra"] . "</td>";
                $html .= "<td>" . $fila["precioVenta"] . "</td>";
                $html .= "<td>" . $fila["stock"] . "</td>";
                $html .= "<td>" . $fila["stock_minimo"] . "</td>";
                $html .= "<td>" . $fila["proveedor"] . "</td>";
                $html .= "<td>" . $estado . "</td>";
                $html .= "<td><a class=\"btn btn-danger \" href=\"javascript:eliminar('" . $fila["codigo"] . "')\"><i class=\"bi bi-cart-x\"></i></a>";
                $html .= "<a class=\"btn btn-warning \" href=\"javascript:editar('" . $fila["codigo"] . "')\"><i class=\"bi bi-pencil-square\"></i></a></td>";
                $html .= "</tr>";
            }
            $html .= "</tbody></table>";
            $res->close();
            $this->desconectar();
        } catch (Exception $ex) {
            http_response_code(400);
            header("Content-Type: application/json");
            return json_encode($ex->getMessage());
        }
        return $html;
    }

    public function fillData($id)
    {
        //EJEMPLO DE CONSULTAS NO PREPARADAS
        $sql = "select a.codigo, a.nombre, a.precioCompra, a.precioVenta, a.stock, a.stock_minimo, b.codigo as proveedor, a.estado from producto a ";
        $sql .= "inner join proveedor b on a.id_proveedor = b.codigo where a.codigo = $id";
        $this->conectar();
        $dataArray = array();
        try {
            $res = $this->con->query($sql);
            while ($fila = mysqli_fetch_assoc($res)) {
                $dataArray[] = $fila;
            }
            $res->close();
            $this->desconectar();
        } catch (Exception $ex) {
            http_response_code(400);
            header("Content-Type: application/json");
            return json_encode($ex->getMessage());
        }
        header("Content-Type: application/json");
        return json_encode($dataArray);
    }

    public function getProveedores()
    {
        //CONSULTA NO PREPARADA
        $sql = "select codigo, nombre from proveedor ";
        $html = "";
        try {
            $this->conectar();
            $res = $this->con->query($sql);
            $html .= "<option selected value='' disabled>Seleccione ...</option>";
            while ($fila = mysqli_fetch_assoc($res)) {
                $html .= "<option value='" . $fila["codigo"] . "'>" . $fila["nombre"] . "</option>";
            }
            $this->desconectar();
            $res->close();
        } catch (Exception $ex) {
            http_response_code(400);
            header("Content-Type: application/json");
            return json_encode($ex->getMessage());
        }
        return $html;
    }

    public function insertar(Producto $prod)
    {
        //USANDO CONSULTAS PREPARADAS
        try {
            $this->conectar();
            //crear la sentencia preparada
            $pstm = $this->con->prepare("insert into producto(nombre, precioCompra, precioVenta, stock, stock_minimo, id_proveedor, estado) values(?,?,?,?,?,?,?)");
            //establecer parametros al PreparedStatement
            $nombre = $prod->getNombre();
            $precioCompra = $prod->getPrecioCompra();
            $precioVenta = $prod->getPrecioVenta();
            $stock = $prod->getStock();
            $stockMinimo = $prod->getStockMinimo();
            $proveedor = $prod->getIdProveedor();
            $estado = $prod->getEstado();
            //incluirlos en la consulta (establecer parametros)
            //s = string
            // i = integer
            // d = digit   (12.365)
            // b = boolean    true, false
            $pstm->bind_param("sddiiii",
                $nombre,
                $precioCompra,
                $precioVenta,
                $stock,
                $stockMinimo,
                $proveedor,
                $estado
            );

            //ejecutar la sentencia
            if ($pstm->execute()) {
                $pstm->close();
                $this->desconectar();
                return true;
            } else {
                $pstm->close();
                $this->desconectar();
                return false;
            }

        } catch (Exception $ex) {
            http_response_code(400);
            header("Content-Type: application/json");
            return json_encode($ex->getMessage());
        }
    }


    public function eliminar($codigo)
    {
        //USANDO CONSULTA PREPARADA
        $this->conectar();
        $pstm = $this->con->prepare("delete from producto where codigo=?");
        $pstm->bind_param("i", $codigo);
        if ($pstm->execute()) {
            $pstm->close();
            $this->desconectar();
            return true;
        } else {
            $pstm->close();
            $this->desconectar();
            return false;
        }
    }

    public function modificar(Producto $prod)
    {
        //USANDO CONSULTAS PREPARADAS
        try {
            $this->conectar();
            //crear la sentencia preparada
            $pstm = $this->con->prepare("update producto set nombre = ?, precioCompra = ?, precioVenta = ?, stock = ?, stock_minimo = ?, id_proveedor = ?, estado = ? where codigo = ?");
            //establecer parametros al PreparedStatement
            $codigo = $prod->getCodigo();
            $nombre = $prod->getNombre();
            $precioCompra = $prod->getPrecioCompra();
            $precioVenta = $prod->getPrecioVenta();
            $stock = $prod->getStock();
            $stockMinimo = $prod->getStockMinimo();
            $proveedor = $prod->getIdProveedor();
            $estado = $prod->getEstado();
            //incluirlos en la consulta (establecer parametros)
            // s = string
            // i = integer
            // d = digit   (12.365)
            // b = boolean    true, false
            $pstm->bind_param("sddiiiii",
                $nombre,
                $precioCompra,
                $precioVenta,
                $stock,
                $stockMinimo,
                $proveedor,
                $estado,
                $codigo
            );

            //ejecutar la sentencia
            if ($pstm->execute()) {
                $pstm->close();
                $this->desconectar();
                return true;
            } else {
                $pstm->close();
                $this->desconectar();
                return false;
            }

        } catch (Exception $ex) {
            http_response_code(400);
            header("Content-Type: application/json");
            return json_encode($ex->getMessage());
        }
    }
}

?>