function cargarTablaFull() {
    $.ajax({
        url: '../../Controller/Producto/ProductoController.php',
        type: 'post',
        data: {key: 'getAll'}
    }).done(function (resultado) {
        console.log(resultado);
        $("#selectEstadoFiltro").val("");
        llenarTabla(resultado);
    }).fail(function (error) {
        console.log(error)
        Swal.fire({
            icon: 'error',
            title: 'Error',
            text: 'Error al cargar la informacion',
        });
    });
}
function llenarTabla(resultado){
    let tabla = $('#tabla');
    tabla.DataTable({
        data: resultado,
        columns: [
            {
                title: "Codigo",
                data: "codigo",
            },
            {
                title: "Nombre",
                data: "nombre"
            },
            {
                title: "Costo",
                data: "precioCompra"
            },
            {
                title: "Precio",
                data: "precioVenta"
            },
            {
                title: "Stock",
                data: "stock",
            },
            {
                title: "Stock Minimo",
                data: "stock_minimo"
            },
            {
                title: "Codigo Proveedor",
                data: "id_proveedor"
            },
            {
                title: "Estado",
                data: "estado"
            },
            {
                title: "Acciones",
                data: null,
                render:function (data, type, row) {
                    if(data.estado===1){
                        return `<input id="btnDesactivar"  class="btn btn-danger me-1" type="submit" value="Desactivar"/>`
                    }else{
                        return `<input id="btnActivar" class="btn btn-success me-1" type="submit" value="Activar"/>`
                    }
                }
            }
        ],
        columnDefs: [
            {
                targets: [7],
                render: function (data) {
                    if (data === 1) {
                        return '<text>Activo</text>';
                    } else {
                        return '<text>Inactivo</text>';
                    }
                }
            },
        ],
        stateSave: true,
        "bDestroy": true,
        searching: true,
        language: {
            "info": "Mostrando _START_ a _END_ de _TOTAL_ Registros",
            "lengthMenu": "Mostrar _MENU_ registros",
            "Search": "Buscar",
            "paginate": {
                "first": "Primero",
                "last": "Última",
                "next": "Siguiente",
                "previous": "Anterior"
            }
        },
        order: [],
    });

    $('#tabla tbody').on('click', 'input', function () {
        let botonPresionado = this;
        let data = tabla.DataTable().row($(this).parents('tr')).data();
        if (botonPresionado.id === "btnDesactivar") {
            updateProductoEstado(data.codigo,0)
        }else{
            updateProductoEstado(data.codigo,1)
        }
    });
}

function updateProductoEstado(codigo,estado){
    console.log(codigo);
    console.log(estado);
    Swal.fire({
        title: 'Esta seguro que modificar el estado del producto?',
        showDenyButton: true,
        confirmButtonText: 'Modificar',
        denyButtonText: `Cancelar`,
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: '../../Controller/Producto/ProductoController.php',
                type: 'post',
                data: {key: 'updateEstado', codigo: codigo, estado: estado}
            }).done(function (resp) {
                console.log(resp);
                if (resp === true) {
                    Swal.fire({
                        icon: 'success',
                        title: 'Producto modificado',
                        text: `Se actualizo el estado del producto`,
                    });
                    cargarTablaFull();
                    $("#btnCerrarModal").click();
                } else {
                    Swal.fire({
                        icon: 'error',
                        title: 'Error',
                        text: `Ocurrio un error inesperado al cambiar el estado del producto`,
                    });
                    cargarTablaFull();
                }

            }).fail(function (error) {
                console.log(error)
                Swal.fire({
                    icon: 'error',
                    title: 'Error',
                    text: `Error al cambiar el estado`,
                });
            });
        }
    })
}

function getProductoByState(data){
    console.log(data);
    $.ajax({
        url: '../../Controller/Producto/ProductoController.php',
        type: 'post',
        data: {key: 'getByState', estado: data}
    }).done(function (resp) {
        console.log(resp);
        llenarTabla(resp)
    }).fail(function (error) {
        console.log(error)
        Swal.fire({
            icon: 'error',
            title: 'Error',
            text: `Error al filtrar los productos`,
        });
    });
}

$(document).ready(function () {
    cargarTablaFull();

    $('#selectEstadoFiltro').on('change', function () {
        let selectVal = $("#selectEstadoFiltro option:selected").val();
        if(selectVal==="1"){
            getProductoByState(1);
        }else if(selectVal==="0"){
            getProductoByState(0);
        }else{
            cargarTablaFull();
        }
    });
});
