function cargarTablaFull() {
    $.ajax({
        url: '../../Controller/Producto/ProductoController.php',
        type: 'post',
        data: {key: 'getAll'}
    }).done(function (resultado) {
        console.log(resultado);
        $("#selectEstadoFiltro").val("");
        llenarTabla(resultado);
    }).fail(function (error) {
        console.log(error)
        Swal.fire({
            icon: 'error',
            title: 'Error',
            text: 'Error al cargar la informacion',
        });
    });
}

async function fillModal(data) {
    $("#form1")[0].reset();  //limpiar el form
    $("#txtCodigo").show();
    $("#exampleModalLabel").text(`Modificacion producto`);
    $("#btnGuardar").hide();
    $("#btnModificar").show();
    $("#txtNombre").val(data.nombre);
    $("#txtCosto").val(data.precioCompra);
    $("#txtPrecio").val(data.precioVenta);
    $("#txtStock").val(data.stock);
    $("#txtStockMinimo").val(data.stock_minimo);
    $("#txtCodigo").val(data.codigo);
    await cargarSelectProveedor();
    $("#selectProveedor").val(data.id_proveedor).change();
}

function insertProducto(data) {
    console.log(data);
    $.ajax({
        url: '../../Controller/Producto/ProductoController.php',
        type: 'post',
        data: {key: 'insert', data: data}
    }).done(function (resp) {
        if(resp===true){
            Swal.fire({
                icon: 'success',
                title: 'Registro Guardado',
                text: `Se registro el producto`,
            });
            cargarTablaFull();
            $("#btnCerrarModal").click();
        }else{
            Swal.fire({
                icon: 'error',
                title: 'Error',
                text: `Ocurrio un error inesperado al guardar el producto`,
            });
            cargarTablaFull();
            $("#btnCerrarModal").click();
        }

    }).fail(function (error) {
        console.log(error)
        Swal.fire({
            icon: 'error',
            title: 'Error',
            text: `Error al registrar el producto`,
        });
    });
}

function updateProducto(data) {
    console.log(data);
    Swal.fire({
        title: 'Esta seguro que desea modificar el producto?',
        showDenyButton: true,
        confirmButtonText: 'Modificar',
        denyButtonText: `Cancelar`,
    }).then((result) => {
        if(result.isConfirmed){
            $.ajax({
                url: '../../Controller/Producto/ProductoController.php',
                type: 'post',
                data: {key: 'update', data: data}
            }).done(function (resp) {
                console.log(resp);
                if(resp===true){
                    Swal.fire({
                        icon: 'success',
                        title: 'Registro Modificado',
                        text: `Se actualizo el producto`,
                    });
                    cargarTablaFull();
                    $("#btnCerrarModal").click();
                }else{
                    Swal.fire({
                        icon: 'error',
                        title: 'Error',
                        text: `Ocurrio un error inesperado al modificar el producto`,
                    });
                    cargarTablaFull();
                    $("#btnCerrarModal").click();
                }

            }).fail(function (error) {
                console.log(error)
                Swal.fire({
                    icon: 'error',
                    title: 'Error',
                    text: `Error al modificar`,
                });
            });
        }
    });
}


function deleteProducto(codigo) {
    Swal.fire({
        title: 'Esta seguro que desea eliminar el producto?',
        showDenyButton: true,
        confirmButtonText: 'Eliminar',
        denyButtonText: `Cancelar`,
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: '../../Controller/Producto/ProductoController.php',
                type: 'post',
                data: {
                    key: 'delete',
                    codigo: codigo
                }
            }).done(function (resultado) {
                Swal.fire({
                    icon: 'success',
                    title: 'Registro Eliminado',
                    text: `Se elimino ${resultado} producto`,
                });
                cargarTablaFull();
            }).fail(function (error) {
                console.log(error)
                Swal.fire({
                    icon: 'error',
                    title: 'Error',
                    text: 'Error al eliminar el producto',
                });
            });
        }
    })
}
async function cargarSelectProveedor() {
    var select = $("#selectProveedor");
    await $.ajax({
        url: '../../Controller/Proveedor/ProveedorController.php',
        type: 'post',
        data: {key: 'getAll'}
    }).done(function (resultado) {
        select.empty();
        for (let i = 0; i < resultado.length; i++) {
            let opt = new Option(resultado[i].nombre, resultado[i].codigo);
            select.append(opt);
        }

    }).fail(function () {
        console.log(error)
        Swal.fire({
            icon: 'error',
            title: 'Error',
            text: 'Error al cargar los proveedores',
        });
    });
}

function llenarTabla(resultado){
    let tabla = $('#tabla');
    tabla.DataTable({
        data: resultado,
        columns: [
            {
                title: "Codigo",
                data: "codigo",
            },
            {
                title: "Nombre",
                data: "nombre"
            },
            {
                title: "Costo",
                data: "precioCompra"
            },
            {
                title: "Precio",
                data: "precioVenta"
            },
            {
                title: "Stock",
                data: "stock",
            },
            {
                title: "Stock Minimo",
                data: "stock_minimo"
            },
            {
                title: "Codigo Proveedor",
                data: "id_proveedor"
            },
            {
                title: "Acciones",
                data: null,
                defaultContent: '<input id="btnEliminar" name="delete" class="btn btn-danger me-1" type="submit" value="Eliminar"/><input data-bs-toggle="modal" data-bs-target="#exampleModal" name="modify" type="submit" value="Modificar" class="btn btn-warning"/>'
            }
        ],
        stateSave: true,
        "bDestroy": true,
        searching: true,
        language: {
            "info": "Mostrando _START_ a _END_ de _TOTAL_ Registros",
            "lengthMenu": "Mostrar _MENU_ registros",
            "Search": "Buscar",
            "paginate": {
                "first": "Primero",
                "last": "Última",
                "next": "Siguiente",
                "previous": "Anterior"
            }
        },
        order: [],
    });

    $('#tabla tbody').on('click', 'input', function () {
        let botonPresionado = this;
        let data = tabla.DataTable().row($(this).parents('tr')).data();
        if (botonPresionado.id === "btnEliminar") {
            deleteProducto(data.codigo);
        }else{
            fillModal(data)
        }
    });
}


$(document).ready(function () {
    cargarTablaFull();
    $("#btnNuevo").on("click", function () {
        cargarSelectProveedor();
        $("#btnModificar").hide();
        $("#btnGuardar").show();
        $("#txtCodigo").hide();
        $("#exampleModalLabel").text("Nuevo Producto");
        $("#form1")[0].reset();  //limpiar el form
    });

    $("#btnGuardar").on("click", function () {
        let stock = $("#txtStock").val();
        let stockMin = $("#txtStockMinimo").val();
        let formulario = $("#form1");
        console.log(stock);
        console.log(stockMin);
        if(formulario[0].checkValidity()){
            if(parseInt(stock)>=parseInt(stockMin)){
                let data=formulario.serialize();
                insertProducto(data);
            }else{
                Swal.fire({
                    icon: 'warning',
                    title: 'Advertencia',
                    text: `El stock no puede ser menor que el stock minimo`,
                });
            }
        }else formulario[0].reportValidity();
    })

    $("#btnModificar").on("click", function () {
        let stock = $("#txtStock").val();
        let stockMin = $("#txtStockMinimo").val();
        let formulario = $("#form1");
        if(formulario[0].checkValidity()){
            if(parseInt(stock)>=parseInt(stockMin)){
                let data=formulario.serialize();
                updateProducto(data);
            }else{
                Swal.fire({
                    icon: 'warning',
                    title: 'Advertencia',
                    text: `El stock no puede ser menor que el stock minimo`,
                });
            }
        }else formulario[0].reportValidity();
    })

    $('#selectEstadoFiltro').on('change', function () {
        let selectVal = $("#selectEstadoFiltro option:selected").val();
        if(selectVal==="1"){
            getProductoByState(1);
        }else if(selectVal==="0"){
            getProductoByState(0);
        }else{
            cargarTablaFull();
        }
    });

})