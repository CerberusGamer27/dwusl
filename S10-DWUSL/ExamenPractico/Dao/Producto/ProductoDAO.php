<?php

interface ProductoDAO
{
    public function insertProducto(Producto $producto);
    public function updateProducto(Producto $producto);
    public function deleteProducto($id);
    public function getAllProductos();
    public function getProductByState($estado);
    public function updateProductoEstado($codigo,$estado);
}