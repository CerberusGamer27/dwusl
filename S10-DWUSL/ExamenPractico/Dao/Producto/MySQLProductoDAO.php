<?php

include_once('C:\xampp\htdocs\ExamenPractico\Dao\Producto\ProductoDAO.php');
include_once('C:\xampp\htdocs\ExamenPractico\Model\Producto.php');
class MySQLProductoDAO  implements ProductoDAO
{

    public function __construct()
    {
    }

    public function insertProducto(Producto $producto)
    {
        try {
            $array = json_decode(json_encode($producto),true);
            unset($array["codigo"]);
            $conexion = MySQLFactoryDAO::createConnection();
            $sql= "INSERT INTO producto(nombre,precioCompra,precioVenta,stock,stock_minimo,id_proveedor,estado)"
            ." VALUES (:nombre,:precioCompra,:precioVenta,:stock,:stock_minimo,:id_proveedor,:estado)";
            return json_encode($conexion->prepare($sql)->execute($array));
        }catch (Exception $error){
            return json_encode($error->getMessage());
        } finally {
            $conexion = null;
        }
    }

    public function updateProducto(Producto $producto)
    {
        try {
            $array = json_decode(json_encode($producto),true);
            unset($array["estado"]);
            $conexion = MySQLFactoryDAO::createConnection();
            $sql= "UPDATE producto SET nombre=:nombre,precioCompra=:precioCompra,precioVenta=:precioVenta,stock=:stock,stock_minimo=:stock_minimo,id_proveedor=:id_proveedor"
                ." WHERE codigo=:codigo";
            return json_encode($conexion->prepare($sql)->execute($array));
        }catch (Exception $error){
            return json_encode($error->getMessage());
        } finally {
            $conexion = null;
        }
    }

    public function updateProductoEstado($codigo,$estado)
    {
        try {
            $conexion = MySQLFactoryDAO::createConnection();
            $sql= "UPDATE producto SET estado=?"
                ." WHERE codigo=?";
            return json_encode($conexion->prepare($sql)->execute([$estado,$codigo]));
        }catch (Exception $error){
            return json_encode($error->getMessage());
        } finally {
            $conexion = null;
        }
    }

    public function deleteProducto($id)
    {
        try {
            $conexion = MySQLFactoryDAO::createConnection();
            $sql="DELETE FROM producto WHERE codigo=?";
            $stm=$conexion->prepare($sql);
            $stm->execute([$id]);
            return json_encode($stm->rowCount());
        }catch (Exception $error){
            return json_encode($error);
        } finally {
            $conexion = null;
        }
    }

    public function getAllProductos()
    {
        try {
            $conexion = MySQLFactoryDAO::createConnection();
            $resultado = $conexion->query("SELECT * FROM producto");
            return json_encode($resultado->fetchAll(PDO::FETCH_CLASS,"Producto"));
        }catch (Exception $error){
            return json_encode($error);
        } finally {
            $conexion = null;
        }
    }

    public function getProductByState($estado)
    {
        try {
            $conexion = MySQLFactoryDAO::createConnection();
            $sql="SELECT * FROM producto WHERE estado=?";
            $statement=$conexion->prepare($sql);
            $statement->execute([$estado]);
            return json_encode($statement->fetchAll(PDO::FETCH_CLASS,"Producto"));
        }catch (Exception $error){
            return json_encode($error);
        } finally {
            $conexion = null;
        }
    }
}