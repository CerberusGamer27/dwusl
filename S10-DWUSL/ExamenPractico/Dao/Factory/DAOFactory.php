<?php

include_once('C:\xampp\htdocs\ExamenPractico\Dao\Factory\MySQLFactoryDAO.php');

abstract class DAOFactory
{
    public static int $MYSQL = 1;

    public abstract function getProductoDAO(): ProductoDAO;

    public abstract function getProveedorDAO():ProveedorDAO;

    public static function getDAOFactory($codigoFactory): ?DAOFactory
    {
        switch ($codigoFactory) {
            case self::$MYSQL;
                return new MySQLFactoryDAO();
            default;
                return null;
        }
    }
}