<?php

include_once('C:\xampp\htdocs\ExamenPractico\Dao\Producto\MySQLProductoDAO.php');
include_once('C:\xampp\htdocs\ExamenPractico\Dao\Proveedor\MySQLProveedorDAO.php');
class MySQLFactoryDAO extends DAOFactory
{

    private static string $user = "root";
    private static string $password = "";
    private static string $dsn = "mysql:host=localhost;dbname=dwusl_db";


    public function getProductoDAO(): ProductoDAO
    {
        return new MySQLProductoDAO();
    }

    public function getProveedorDAO(): ProveedorDAO
    {
        return new MySQLProveedorDAO();
    }


    public static function createConnection()
    {
        try {
            return new PDO(self::$dsn,self::$user,self::$password);
        }catch (Exception $error){
            return $error;
        }
    }

}