<?php
include_once('C:\xampp\htdocs\ExamenPractico\Dao\Proveedor\ProveedorDAO.php');
include_once('C:\xampp\htdocs\ExamenPractico\Model\Proveedor.php');
class MySQLProveedorDAO implements ProveedorDAO
{

    public function __construct()
    {
    }

    public function getAllProveedores()
    {
        try {
            $conexion = MySQLFactoryDAO::createConnection();
            $resultado = $conexion->query("SELECT * FROM proveedor");
            return json_encode($resultado->fetchAll(PDO::FETCH_CLASS,"Proveedor"));
        }catch (Exception $error){
            return json_encode($error);
        } finally {
            $conexion = null;
        }
    }
}