<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: *");
header("Access-Control-Allow-Headers: *");
header('Content-type: application/json');
include_once('C:\xampp\htdocs\ExamenPractico\Dao\Factory\DAOFactory.php');

$mysqlFactory = DAOFactory::getDAOFactory(DAOFactory::$MYSQL);;
$productoDAO=$mysqlFactory->getProductoDAO();;

if(isset($_POST['key'])){
    $key=$_POST["key"];
    switch ($key){
        case "getAll":
            echo $productoDAO->getAllProductos();
            break;

        case 'delete':
            echo $productoDAO->deleteProducto($_POST["codigo"]);
            break;

        case 'getByState':
            echo $productoDAO->getProductByState($_POST["estado"]);
            break;

        case 'insert':
            parse_str($_POST["data"],$data);
            $producto = new Producto();
            $producto->setNombre($data["txtNombre"]);
            $producto->setEstado(1);
            $producto->setPrecioCompra($data["txtCosto"]);
            $producto->setPrecioVenta($data["txtPrecio"]);
            $producto->setStock($data["txtStock"]);
            $producto->setStockMinimo($data["txtStockMinimo"]);
            $producto->setIdProveedor($data["selectProveedor"]);
            echo $productoDAO->insertProducto($producto);
            break;

        case 'update':
            parse_str($_POST["data"],$data);
            $producto = new Producto();
            $producto->setNombre($data["txtNombre"]);
            $producto->setCodigo($data["txtCodigo"]);
            $producto->setPrecioCompra($data["txtCosto"]);
            $producto->setPrecioVenta($data["txtPrecio"]);
            $producto->setStock($data["txtStock"]);
            $producto->setStockMinimo($data["txtStockMinimo"]);
            $producto->setIdProveedor($data["selectProveedor"]);
            echo $productoDAO->updateProducto($producto);
            break;

        case 'updateEstado':
            $codigo=$_POST["codigo"];
            $estado=$_POST["estado"];
            echo $productoDAO->updateProductoEstado($codigo,$estado);
            break;


    }
}