create database dwusl_db;
use dwusl_db;
create table proveedor(
codigo int primary key auto_increment,
nombre varchar(100),
nit varchar(100),
tel varchar(100)
);

insert into proveedor(nombre, nit, tel) values('Proveedor 1','123456','22223333');
insert into proveedor(nombre, nit, tel) values('Proveedor 2','123456','22223333');
select * from proveedor;

create table producto(
codigo int primary key auto_increment,
nombre varchar(50),
precioCompra decimal(9,2),
precioVenta decimal(9,2),
stock int,
stock_minimo int,
id_proveedor int,
estado int, -- 0 inactivo  1 activo
foreign key (id_proveedor) references proveedor(codigo)
);
insert into producto(nombre, precioCompra, precioVenta, stock, stock_minimo, id_proveedor, estado)
values
('Maruchan',0.75,1.00,100,20,1,1),
('Coca-Cola',0.50,1.25,100,20,2,1),
('Papas Lays',0.25,0.75,100,20,1,1),
('Atun Pacifico Azul',1.00,1.75,100,20,2,1);
select * from producto;
select * from producto where id_proveedor=1;


-- tablas para el examen 3


create table usuario(
idusuario int auto_increment primary key,
rol varchar(50) not null,     -- vendedor  --administrador 
usuario varchar(50) not null, 
clave varchar(50) not null,
telefono varchar(15),
estado int not null,        -- sera 0 desactivado  1 activo
nombre varchar(100) not null,
email varchar(100)
);

-- SOBRE LOS INGRESOS DE LOS PRODUCTOS
create table ingresos(            
idingreso int auto_increment primary key,
idusuario int not null,   
tipodocumento varchar(30),
numerodocumento varchar(30),
numerocomprobante varchar(30),
fecha date not null,
total float(8,2),
descripcion varchar(100),
foreign key(idusuario) references usuario(idusuario)
on delete cascade on update cascade
);

-- DETALLE DEL INGRESO
create table detalleingreso(
iddetalleingreso int auto_increment primary key,
idingreso int not null, 		
idproducto int not null, 
cantidad int not null,
preciocompra float(8,2) not null,
precioventa float(8,2) not null,
oldPrecioV float(8,2),                         
foreign key(idingreso) references ingresos(idingreso)
on delete cascade on update cascade,
foreign key(idproducto) references producto(idproducto)
on delete cascade on update cascade
);



