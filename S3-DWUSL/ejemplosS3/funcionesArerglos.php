<?php
//VERFICACION DE QUE UNA VARIABLE ES ARRAY
$arreglo = ["item1",2,3];
$variable = "hola mundo!";
echo is_array($arreglo);
echo is_array($variable);

//PASAR UNA CADENA A ARRAY
$cadena = "hola todos, como estan?";
$res = explode(" ",$cadena);
var_dump($res);

//PASAR UN ARRAY A UNA CADENA
$res2 = implode(" ",$res);
echo $res2;

//COMBINAR VALORES DE MAS DE UN ARRAY
$arr1=[1,2,3,4];
$arr2=['cinco','seis','siete'];
$arr3 = array_merge($arr1,$arr2);
var_dump($arr3);

//SACAR LA DIFERENCIA ENTRE ARRAYS
$ar1 = [1,2,3,4,5,6,7];
$ar2 = [2,4,6];
$ar3 = [3,5];
$dif = array_diff($ar1,$ar2,$ar3);
var_dump($dif);

  //con array asociativos
$asoc1 =["nombre"=>"Juan", "apellido"=>"Perez",
         "direcion"=>"San Salvador"];
$asoc2 =["nombre"=>"Juan", "apellido"=>"Perez"];

$asoc3 =array_diff_assoc($asoc1,$asoc2);
var_dump($asoc3);


//  buscar un elemento dentro de un array
$colores = ["rojo","azul","amarillo", "cafe"];
echo "indice de color azul: ". array_search("azul",$colores);

//CONTAR ELEMENTOS DENTRO DE UN ARRAY

echo "el array colores tiene ". count($colores). " items";
$colores[count($colores)] = "ultimo valor";
var_dump($colores);












?>