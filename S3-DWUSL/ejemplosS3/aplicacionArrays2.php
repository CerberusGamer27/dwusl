<?php
/*
CREAR UN ARRAY ASOCIATIVO QUE GAURDE VALORES DE
PRECIO DE ALGUNAS CRIPTOMONEDAS  Y EL OBJETIVO ES
CARGAR DICHO ARRAY ASOCIATIVO A LOS ELEMENTOS DE
UN SELECT HTML
Bitcoin	23642
Ethereum	1824.23
Bitcoin cash	140.6
Litecoin	61.51
*/
$cryptos = ["Bitcoin"=>23642,
            "Ethereum"=>1824.23,
            "Bitcoin cash"=>140.6,
            "Litecoin"=>61.51] ;
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.min.js" integrity="sha384-ODmDIVzN+pFdexxHEHFBQH3/9/vQ9uori45z4JjnFsRydbmQbmL5t1tQ0culUzyK" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>

    <title>Document</title>
</head>
<body>
<h5>Seleccione una cryptomoneda</h5><hr>
<div style="width: 500px; margin: auto;">
    <form method="post" action="aplicacionArrays2.php">
        <select name="cmbCryptos" class="form-control">
            <?php
            foreach ($cryptos as $indice => $valor) {
                ?>
                    <option value="<?=$valor ?>"><?=$indice ?></option>
                <?php
            }
            ?>
        </select><br>
        <input type="submit" name="btnMostrarValor" value="Seleccionar"
        class="btn btn-danger btn-sm">
        <input type="submit" name="btnMostrarTabla" value="Ver Tabla"
               class="btn btn-warning btn-sm">
    </form>

    <?php
    if (isset($_REQUEST["btnMostrarValor"])){
        $valor= $_REQUEST["cmbCryptos"];
        $msj = "<script>alert('valor de moneda seleccionada: $$valor')</script>";
        echo $msj;
    }
    if (isset($_REQUEST["btnMostrarTabla"])){
        ?>
        <table class="table">
            <thead>
                <th> MONEDA</th><th> VALOR $</th>
            </thead>
            <tbody>
            <?php
            foreach ($cryptos as $key=>$value){
                ?>
                <tr>
                    <td><?=$key ?> </td> <td><?=$value ?> </td>
                </tr>
                <?php
            }
            ?>
            </tbody>
        </table>

    <?php

    }


    ?>
</div>

</body>
</html>