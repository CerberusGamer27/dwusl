<?php
/*
CREAR UN PROGRAMA PARA GENERAR CORREOS ELECTRONICOS
CON EL FORMATO juan.perez22@itca.edu.sv
(para Juan Ernesto Perez Paiz)
EN BASE A LOS DOS NOMBRES Y DOS APELLIDOS INGRESADOS
MEDIANTE UN FORMULARIO....
CADA AÑO, EL FORMATO DEBERÍA CAMBIAR DE MANERA
AUTOMATICA
*/
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.min.js" integrity="sha384-ODmDIVzN+pFdexxHEHFBQH3/9/vQ9uori45z4JjnFsRydbmQbmL5t1tQ0culUzyK" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>

    <title>Document</title>
</head>
<body>
<h4>Generador de correos electronicos</h4><hr>
<div style="width: 500px; margin: auto;">
    <form method="post" action="aplicacionArrays.php">
        <input type="text" name="txtNombre" placeholder=" 2 nombres 2 apellidos"
        class="form-control"><br>
        <input type="submit" name="btnGenerar" value="Generar correo"
               class="btn btn-success btn-sm">
       </form>
</div>
<!-- procesar con php -->
<?php

if (isset($_POST["btnGenerar"])){
    $nombre =  $_POST["txtNombre"];
    //apicando explode
    $arr = explode(" ", $nombre);
    //var_dump($arr);
    //encontrando el año actual
    $fecha = new DateTime(); //aqui guardo la fecha completa
    $año = $fecha->format("y");
    //var_dump($año);
    $correo = $arr[0].".".$arr[2] . $año . "@itca.edu.sv";

    echo "<hr> <pre>Correo generado: <b>$correo</b></pre><hr>";




}





?>





</body>
</html>
