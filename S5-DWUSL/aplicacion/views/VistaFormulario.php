<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- bootstrap-->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.min.js" integrity="sha384-ODmDIVzN+pFdexxHEHFBQH3/9/vQ9uori45z4JjnFsRydbmQbmL5t1tQ0culUzyK" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
    <!-- jquery -->
    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>

    <!-- sweet alert -->
    <link href="//cdn.jsdelivr.net/npm/@sweetalert2/theme-dark@4/dark.css" rel="stylesheet">
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11/dist/sweetalert2.min.js"></script>

    <script>
        $(document).ready(function(){
                $("#espacioPara").hide();

                $("#btnProcesar").on("click", function(){
                    var dataFormulario = $("#formulario").serialize();
                    $.ajax({
                        url: '../controller/Controller.php',
                        type: 'post',
                        data: {key:'recibe',dataform : dataFormulario}
                    }).done(function(respuesta){
                        $("#espacioPara").empty();
                        $("#espacioPara").append(respuesta);
                        $("#espacioPara").show()
                        swal.fire("Datos fueron procesados ...")

                    }).fail(function(){
                        swal.fire("Datos NO fueron procesados ...")
                    });



                });







        });


    </script>


    <title>Ejemplo MVC + Ajax</title>
</head>
<body>
<br>
<h4 align="center">Ejercicio de aplicacion POO - JS(Ajax)  - MVC </h4>
<hr>
<div id="contenedor" style="margin: auto; width: 50%" class="small">
    <form id="formulario" class="form-group">
        <input type="text" id="txtNombres" name="txtNombres"
                class="form-control" placeholder="Ingrese sus nombres"><br>
        <input type="text" id="txtApelldios" name="txtApellidos"
                class="form-control" placeholder="Ingrese sus apellidos"><br>
        Genero: <br>
        <input type="radio" id="rdGenero" name="rdGenero" value="Masculino"
               class="form-check-input"> Masculino
        <input type="radio" id="rdGenero" name="rdGenero"  value="Femenino"
               class="form-check-input"> Femenino  <br><br>
        Preferencias : <br>
        video Juegos <input type="checkbox" id="ckPreferencias[]"
                            name="ckPreferencias[]" value="Video Juegos" class="form-check-input">
        Musica <input type="checkbox" id="ckPreferencias[]"
                            name="ckPreferencias[]" value="Video Juegos" class="form-check-input">
        Deportes <input type="checkbox" id="ckPreferencias[]"
                      name="ckPreferencias[]" value="Video Juegos" class="form-check-input">
        <br><br>
        Estado civil <br>
        <select id="cmbEstadoCivil" name="cmbEstadoCivil" class="form-control">
            <option value="Solter@">Solter@</option>
            <option value="Casad@">Casad@</option>
            <option value="Viud@">Viud@</option>
        </select>
        <br><br>
        <input type="button" value="Procesar" class="btn btn-success"
                    id="btnProcesar" name="btnProcesar">

    </form>

    <br>
    <br>
    <div id="espacioPara">
        <h5>Datos procesados</h5><hr>

    </div>


</div>








</body>
</html>