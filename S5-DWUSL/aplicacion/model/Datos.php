<?php
class Datos{
    private $nombres;
    private $apellidos;
    private $genero;
    private $prefencias;
    private $estadoCivil;

    function __construct()
    {
        //no code
    }

    public function getNombres()
    {
        return $this->nombres;
    }

    public function setNombres($nombres)
    {
        $this->nombres = $nombres;
        return $this;
    }

    public function getApellidos()
    {
        return $this->apellidos;
    }

    public function setApellidos($apellidos)
    {
        $this->apellidos = $apellidos;
        return $this;
    }

    public function getGenero()
    {
        return $this->genero;
    }

    public function setGenero($genero)
    {
        $this->genero = $genero;
        return $this;
    }

    public function getPrefencias()
    {
        return $this->prefencias;
    }

    public function setPrefencias($prefencias)
    {
        $this->prefencias = $prefencias;
        return $this;
    }

    public function getEstadoCivil()
    {
        return $this->estadoCivil;
    }

    public function setEstadoCivil($estadoCivil)
    {
        $this->estadoCivil = $estadoCivil;
        return $this;
    }



}



?>
