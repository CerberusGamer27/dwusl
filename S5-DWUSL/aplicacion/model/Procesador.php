<?php
require_once 'Datos.php';

class Procesador{

    function recibe(Datos $form){
        $html="<table class='table table-striped table-bordered'><thead><th>Campo</th><th>Valor</th></thead>";
        $html .= "<tbody>";
        $html .= "<tr>";
            $html .= "<td>Nombres</td>";
            $html .= "<td>" . $form->getNombres() . "</td>";
        $html .= "</tr>";
        $html .= "<tr>";
            $html .= "<td>Apellidos</td>";
            $html .= "<td>" . $form->getApellidos() . "</td>";
        $html .= "</tr>";
        $html .= "<tr>";
            $html .= "<td>Genero</td>";
            $html .= "<td>" . $form->getGenero() . "</td>";
        $html .= "</tr>";
        $html .= "<tr>";
            $html .= "<td>Estado Civil</td>";
            $html .= "<td>" . $form->getEstadoCivil() . "</td>";
        $html .= "</tr>";
        $html .= "<tr>";
            $html .= "<td>Preferencias</td>";
            $html .= "<td>";
                $html .= "<ul>";
                    foreach ($form->getPrefencias() as $item=>$value){
                        $html .= "<li>" . $value . "</li>";
                    }
                $html .= "</ul>";
            $html .= "</td>";
        $html .= "</tr></tbody></table>";
        return $html;
    }





}
