<?php
/*
*****************************POO************
* ABSTRACCION: sacar las caracteristicas de un
                objeto del mundo real y plasmarlas
                en una clase.

* ENCAPSULAMIENTO: significa ocultacion de datos...

* HERENCIA:  proceso mediante el cual se transmiten
            atributos, metodos y otros miembros de una
            clase padre hacia una clase hija.

* POLIMORFISMO:  significa "multiples formas", es la
                capacidad de representar una misma
                propiedad (de una clase) de distintas
                formas, el polimorfismo se aplicar por
                sobre carga de metodos y sobre escritura de
                metodos
*/

//abstraccion: un carro
//para que: para un sistema de venta de vehiculos usados

class Vehiculo{
    //atributos
    private $marca;
    private $modelo;
    private $año;
    protected $precioCopra;
    protected $precioVenta;
    private $aumento_precio;
    public function __construct($aumento_precio)
    {
        $this->aumento_precio = $aumento_precio/100;
    }
    //metodos
    function vender(){
        //codigo
    }
    function comprar(){
        //codigo
    }

    //GETTER Y SETTER
    public function getMarca(){
        return $this->marca;
    }
    public function setMarca($marca){
        $this->marca=$marca;
    }
    public function getModelo()
    {
        return $this->modelo;
    }
    public function setModelo($modelo)
    {
        $this->modelo = $modelo;
        return $this;
    }
    public function getAño()
    {
        return $this->año;
    }
    public function setAño($año)
    {
        $this->año = $año;
        return $this;
    }
    public function getPrecioCopra()
    {
        return $this->precioCopra;
    }
    public function setPrecioCopra($precioCopra)
    {
        $this->precioCopra = $precioCopra;
        return $this;
    }
    public function getPrecioVenta()
    {
        $aumento = $this->precioCopra * $this->aumento_precio;
         $this->precioVenta = $this->precioCopra + $aumento;
         return $this->precioVenta;
    }
    public function setPrecioVenta($precioVenta)
    {
        $this->precioVenta = $precioVenta;
        return $this;
    }
}

class Moto extends Vehiculo {
    private $cilindraje;
    function __construct($aumento_precio)
    {
        parent::__construct($aumento_precio);
    }
    public function getCilindraje()
    {
        return $this->cilindraje;
    }
    public function setCilindraje($cilindraje)
    {
        $this->cilindraje = $cilindraje;
        return $this;
    }

    public function getPrecioVenta()
    {
        $this->precioVenta = $this->precioCopra * 1.50;
        return $this->precioVenta;
    }
    function getData($arg1){

    }
    /*function getData($arg1, $arg2){
                //no aplica sobre cargar de metodos
    }*/



}


//TEST DE LA CLASE
echo "<h5>Objeto Vehiculo </h5><hr>";
$obj = new Vehiculo(20);
$obj->setMarca("Ford");
$obj->setPrecioCopra(6000);
echo "Marca vehiculo: " . $obj->getMarca() ."<br>";
echo "Precio Venta: $" . $obj->getPrecioVenta();

echo "<h5>Objeto Moto </h5><hr>";
$moto = new Moto(80);
$moto ->setMarca("Suzuki");
$moto->setModelo("Katana");
$moto->setPrecioCopra(2000);
echo "Marca: " . $moto->getMarca() ."<br>";
echo "Modelo: " . $moto->getModelo();
echo "Precio Venta $". $moto->getPrecioVenta();










?>