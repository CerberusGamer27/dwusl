<?php
session_start();
$_SESSION["usuarios"]="123";
setcookie("galleta", "valor");

function evitarXss(&$string,$low=false){
    if(!is_array($string)){
        $string = trim($string);
        $string = strip_tags($string);
        $string = htmlspecialchars($string);
        if($low){
            return true;
        }
        $string = str_replace(array('"','\\',"'","/","..","./","//"),'',$string);
        $no = '/%0[0-8bcef]/';
        $string = preg_replace($no, '',$string);
        $no = '/%1[0-9a-f]/';
        $string = preg_replace($no, '',$string);
        $no = '/[\x00-\x08\x0B\x0C\x0E-\x1F\x7F]+/s';
        $string = preg_replace($no, '',$string);
        return $string;
    }

    $keys = array_keys($string);
    foreach ($keys as $key){
        evitarXss($string[$key]);
    }
}





?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.min.js" integrity="sha384-ODmDIVzN+pFdexxHEHFBQH3/9/vQ9uori45z4JjnFsRydbmQbmL5t1tQ0culUzyK" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.6.1.js" integrity="sha256-3zlB5s2uwoUzrXK3BT7AX3FyvojsraNFxCc2vC/7pNI=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js" integrity="sha512-AA1Bzp5Q0K1KanKKmvN/4d3IRKVlv9PYgwFPvm32nPO6QS8yH1HO7LbgB1pgiOxPtfeg5zEn2ba64MUcqJx6CA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>


    <title>XSS</title>
</head>
<body>
<div style="margin: auto; width: 60%">
    <h2>Un simple formulario, una vulnerabilidad</h2><hr>
    <br>
    <h4>Formulario de Logueo</h4>
    <hr>
    <form action="formulario.php" method="post">
        <input type="text" name="txtUsuario" id="txtUsuario"
        class="form-control text-info form-control-lg" placeholder="Usuario">
        <input type="password" name="txtPwd" id="txtPwd"
               class="form-control text-info form-control-lg" placeholder="Password">
        <br>
        <input type="submit" name="btnIngresar" id="btnIngresar" value="Ingresar"
        class="btn btn-outline-dark btn-lg">
    </form>

    <?php
    if(isset($_POST["btnIngresar"])){
        $nombre= evitarXss($_POST["txtUsuario"]);
        $pwd = evitarXss($_POST["txtPwd"]);

        echo "Datos ingresados: <br>";
        echo "Nombre: " . $nombre."<br>";
        echo "Clave: " .  $pwd;
    }
    ?>



</div>
</body>
</html>