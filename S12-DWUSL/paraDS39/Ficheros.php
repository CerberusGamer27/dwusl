<?php
if(true){
    $menu = file_get_contents("menu.html");
    echo $menu;
}else{
    echo "otro menu";
}


function verCarpeta($cual){ //arg= ruta que quiere ver, debe ser capeta
    if(is_dir($cual)) {
        //abrir el gestor de directorios
        $gestor = opendir($cual);
        echo "<div style='margin: auto; width: 400px;'><ul>";
        echo "<label class='badge bg-danger'>Archivos subidos</label>";
        //recorrer todo el directorio para listar archivos
        while (($archivo = readdir($gestor)) !== false) {
            $ruta_completa = $cual . "/" . $archivo;
            //mostrar todo menos . y ..
            if ($archivo != "." && $archivo != "..") {
                //si es directorio lo que se encuentra
                if (is_dir($ruta_completa)) {
                    echo "<li>" . $archivo . "</li>";
                    verCarpeta($ruta_completa);
                } else {
                    //viendo cada archivo normalmente
                    echo "<li>" . $archivo . "</li>";
                    //viendo cada archivo con enlace para abrir en el navegador
                    //echo "<li><a href='Subidas/".$archivo."'>" . $archivo . "</a></li>";

                    //viendo los enlaces de descarga
                    $extension = pathinfo("Subidas/" . $archivo,PATHINFO_EXTENSION);
                    echo "<a href='Subidas/Descargas.php?ext=$extension&nombre=$archivo'>Descargar</a>";



                }
            }
        }
        closedir($gestor);
        echo "</ul></div>";
    }else{
        echo "<script>swal('Directorio no valido')</script>";
    }
}


?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.min.js" integrity="sha384-ODmDIVzN+pFdexxHEHFBQH3/9/vQ9uori45z4JjnFsRydbmQbmL5t1tQ0culUzyK" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.6.1.js" integrity="sha256-3zlB5s2uwoUzrXK3BT7AX3FyvojsraNFxCc2vC/7pNI=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js" integrity="sha512-AA1Bzp5Q0K1KanKKmvN/4d3IRKVlv9PYgwFPvm32nPO6QS8yH1HO7LbgB1pgiOxPtfeg5zEn2ba64MUcqJx6CA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    <title>FICHEROS</title>
</head>
<body>

<h3>SUBIDA AL SERVER</h3><hr>

<form method="post" action="Ficheros.php"
   enctype="multipart/form-data">
    <div class="col-6" style="margin: auto;">
        <label>Seleccione algo</label>
        <input type="file" class="form-control form-control-lg"
        name="archivo"><br>
        <input type="submit" class="btn btn-outline-dark"
        name="btnSubir" value="Subir archivo">
    </div>
</form>
<br>

<?php

verCarpeta("Subidas");

if(isset($_POST["btnSubir"])){
    $achivo = $_FILES["archivo"];  //RECIBIENDO EL ARCHIVO
    $nombre = $_FILES["archivo"]["name"];
    $tipo = $_FILES["archivo"]["type"];
    $tamaño = $_FILES["archivo"]["size"];
    $nomTemp = $_FILES["archivo"]["tmp_name"];
    $destino = "Subidas/" .$nombre;
    /* if(!file_exists("Subidas")){
        mkdir("Subidas",0777,true);
    }*/

    $nombrecampos = explode(".",$nombre);
    $extension= strtolower(end($nombrecampos));
    $tiposPermitidos = array('jpg','gif','png','txt','html','pdf');

    if(in_array($extension, $tiposPermitidos)){
        //funcion para subir archivo al servidor
        if(move_uploaded_file($nomTemp, $destino)){
            echo "<script>swal('Subido con éxito')</script>";
        }else{
            echo "<script>swal('Error al subir')</script>";
        }
    }else{
        echo "<script>swal('Archivo no valido')</script>";
    }
}


?>

<h3>CREACION DE FICHEROS</h3><hr>
<form action="Ficheros.php" method="post">
    <textarea name="texto" rows="4" cols="50"
    class="form-control form-control-lg text-info">

    </textarea>
    <br>
    <input type="submit" class="btn btn-outline-dark btn-lg"
    name="btnTexto" value="Crear Fichero">
</form>
<?php
if(isset($_POST["btnTexto"])){
    $texto = $_POST["texto"];
    $archivo = fopen("Subidas/datos.txt","w");

    /*
    r=solo lectura
    r+= lectura y escritura
    w= solo escritura (reemplaza archivos existentes)
    w+= escritura y lectura
    a= solo escritura
    x= solo escritura
    x+ = escritura y lectura
    */
    fwrite($archivo, $texto);

    fflush($archivo);

    fclose($archivo);
    echo "<script>swal('fichero creado')</script>";

}

?>


<h3>CARGA DE FICHEROS</h3><hr>
<form action="Ficheros.php" method="post">
    <input type="text" value="Subidas/datos.txt"
           class="form-control form-control-lg" name="fichero"><br>
    <input type="submit" class="btn btn-outline-dark btn-lg"
           name="btnTextoCargado" value="Cargar Fichero"><br>
    <textarea name="textoCargado" rows="4" cols="50"
              class="form-control form-control-lg text-info">
        <?php
            if(isset($_POST["btnTextoCargado"])){
                $fichero = $_POST["fichero"];
                $contenido = file_get_contents($fichero);
                echo $contenido;
            }

        ?>
    </textarea>
    <br>

</form>




<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
</body>
</html>