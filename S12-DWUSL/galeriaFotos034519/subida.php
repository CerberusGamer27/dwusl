<!doctype html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3"
            crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.9.1/font/bootstrap-icons.css">
    <script src="https://code.jquery.com/jquery-3.6.1.min.js"
            integrity="sha256-o88AwQnZB+VDvE9tvIXrMQaPlFFSUTR+nldQm1LuPXQ=" crossorigin="anonymous"></script>
    <script type="text/javascript" src="assets/logout.js"></script>
    <link rel="stylesheet" href="styles.css">
    <title>Album de Fotos</title>
</head>
<body>
<div class="container">
    <div class="shadow p-3 mt-5 bg-body rounded">
        <h1 class="text-center">Ingrese las fotos para mostrar en la galeria</h1>
        <hr>
        <form method="post" action="subida.php"
              enctype="multipart/form-data">
            <div class="col-6" style="margin: auto;">
                <div class="mb-3">
                    <label for="archivo" class="form-label">Large file input example</label>
                    <input class="form-control form-control-lg" id="archivo" name="archivo" type="file" required>
                </div>
                <div class="mb-3">
                    <input type="submit" class="btn btn-outline-dark"
                           name="btnSubir" value="Subir archivo">
                </div>
            </div>
        </form>
    </div>
    <?php
    if (isset($_POST["btnSubir"])) {
        $achivo = $_FILES["archivo"];  //RECIBIENDO EL ARCHIVO
        $nombre = $_FILES["archivo"]["name"];
        $tipo = $_FILES["archivo"]["type"];
        $tamaño = $_FILES["archivo"]["size"];
        $nomTemp = $_FILES["archivo"]["tmp_name"];
        $destino = "subidas/" . $nombre;
        /* if(!file_exists("Subidas")){
            mkdir("Subidas",0777,true);
        }*/

        $nombrecampos = explode(".", $nombre);
        $extension = strtolower(end($nombrecampos));
        $tiposPermitidos = array('jpg', 'gif', 'png', 'svg');

        if (in_array($extension, $tiposPermitidos)) {
            //funcion para subir archivo al servidor
            if (move_uploaded_file($nomTemp, $destino)) {
                echo "<script>Swal.fire({
                    icon: 'success',
                    title: 'Correcto',
                    text: `Archivo Subido Correctamente`
                })</script>";
            } else {
                echo "<script>Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Ocurrio un error inesperado, intentalo de nuevo mas tarde',
            })</script>";
            }
        } else {
            echo "<script>
                Swal.fire({
                    icon: 'warning',
                    title: 'Oops...',
                    text: 'Extension del Archivo no Valida',
                })
              </script>";
        }
    }
    ?>

    <?php
    function verCarpeta($cual)
    { //arg= ruta que quiere ver, debe ser capeta
        if (is_dir($cual)) {
            //abrir el gestor de directorios
            $gestor = opendir($cual);
            //recorrer todo el directorio para listar archivos
            while (($archivo = readdir($gestor)) !== false) {
                $ruta_completa = $cual . "/" . $archivo;
                //mostrar todo menos . y ..
                if ($archivo != "." && $archivo != "..") {
                    //si es directorio lo que se encuentra
                    if (is_dir($ruta_completa)) {
                        echo "<div class=\"gallery\">
                                    <a target=\"_blank\" href=\"" . $ruta_completa . "\">
                                        <img src=\"" . $ruta_completa . "\" width=\"600\" height=\"400\">
                                    </a>
                                        <div class=\"desc\">$archivo</div>
                                </div>";
                        verCarpeta($ruta_completa);
                    } else {
                        echo "<div class=\"gallery\">
                                    <a target=\"_blank\" href=\"" . $ruta_completa . "\">
                                        <img src=\"" . $ruta_completa . "\" width=\"600\" height=\"400\">
                                    </a>
                                    <div class=\"desc\">$archivo</div>
                                </div>";

                        //viendo los enlaces de descarga
                        //$extension = pathinfo("Subidas/" . $archivo, PATHINFO_EXTENSION);
                        //echo "<a href='Subidas/Descargas.php?ext=$extension&nombre=$archivo'>Descargar</a>";


                    }
                }
            }
            closedir($gestor);
        } else {
            echo "<script>Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Directorio No Valido',
            })</script>";
        }
    }

    verCarpeta("subidas");
    ?>
</div>
</body>
</html>