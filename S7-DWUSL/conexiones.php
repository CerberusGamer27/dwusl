<?php

$servidor = "141.148.161.172";
$usuario = "cerberus";
$clave= "pass";
$base_de_datos = "viajes";

//CONEXION MEDIANTE MYSQLI
$conn = new mysqli($servidor, $USER, $clave, $base_de_datos);
if(!$conn->errno){
    echo "Conexion exitosa...";
    echo "Info server: <br>";
    echo $conn->host_info;
    echo "<br><br>Info de tabla <b>user</b> de la base de datos mysql <br><hr>";

    $sql = "select nombre, mensaje from testimoniales";
    $result = $conn->query($sql);
    while ($fila = mysqli_fetch_assoc($result)){
        echo $fila["nombre"] . " - " . $fila["mensaje"]. "<br>";
        // $html .= "<option value='".$fila["Host"]."'>".$fila["User"]."</option>";
    }
    $result->close();
    $conn->close();

    //Conexion a MySQL usando PDO
    echo "<br><hr>PDO<hr>";
    try {
        $conexion = new PDO("mysql:host={$servidor};dbname={$base_de_datos}", $usuario, $clave);
        $sql = "select nombre, mensaje from testimoniales";
        foreach ($conexion->query($sql) as $fila){
            echo $fila["nombre"] . " - " . $fila["mensaje"]. "<br>";
        }
    } catch (PDOException $error){
        echo "Error: {$error->getMessage()}";
    }
} else {
    echo "Error";
    die();
}