<?php
require_once 'config.php';
function getProductos()
{
    $con = new mysqli(SERVER, USER, PASS, DB);
    $sql = "select * from producto";
    $res = $con->query($sql);
    $html = "<table class='table table-striped' id='resultado' style='width:100%'><thead>";
    $html .= "<th>CODIGO</th><th>NOMBRE</th><th>STOCK</th><th>COSTO</th><th>ACCION</th></thead><tbody>";
    while ($fila = mysqli_fetch_assoc($res)) {
        $html .= "<tr>";
        $html .= "<td>" . $fila["codigo"] . "</td>";
        $html .= "<td>" . $fila["nombre"] . "</td>";
        $html .= "<td>" . $fila["stock"] . "</td>";
        $html .= "<td>" . $fila["costo"] . "</td>";
        $html .= "<td>";
        $html .= "<a href='#' class='btn btn-primary' data-bs-toggle=\"modal\" data-bs-target=\"#exampleModal\" href='#' onclick=\"javascript:cargarMo('".$fila["codigo"] ."','".$fila["nombre"] ."','".$fila["stock"] ."','".$fila["costo"] ."')\">Modificar</a> | ";
        $html .= "<a href='#' class='btn btn-warning' onclick=\"eliminar('${fila['codigo']}')\">Eliminar</a> | ";
        $html .= "</td>";
        $html .= "</tr>";
    }
    $html .= "</tbody></table>";
    $res->close();
    $con->close();
    return $html;

}

// Agregar Producto
if(isset($_POST["btnGuardar"])){
    $codigo = $_POST["txtCodigo"];
    $nombre = $_POST["txtNombre"];
    $stock = $_POST["txtStock"];
    $costo = $_POST["txtCosto"];
    $con = $con = new mysqli(SERVER, USER, PASS, DB);
    $sql = "INSERT INTO producto VALUES($codigo,'$nombre',$stock,$costo)";
    if($con->query($sql)){
        echo "<script>alert('Registro guardado')</script>";
    } else {
        echo "<script>alert('No se pudo guardar')</script>";
    }
}

// Editar Producto
if(isset($_POST["btnEditar"])){
    $codigo=$_POST["txtCodigo"];
    $nombre = $_POST["txtNombre"];
    $stock = $_POST["txtStock"];
    $costo = $_POST["txtCosto"];
    $con = new mysqli (SERVER,USER,PASS,DB);
    $sql ="update producto set nombre='$nombre', stock=$stock, costo=$costo where codigo=$codigo";
    if($con->query($sql)){
        echo "<script>alert('Registro Modificado')</script>";
    }else{
        echo "<script>alert('No se pudo modificar....')</script>";
    }
    $con->close();
}

// Eliminar
if(isset($_POST["btnEliminar"])){
    $codigo=$_POST["txtCodigo"];
    $con = new mysqli (SERVER,USER,PASS,DB);
    $sql ="delete from producto where codigo=$codigo";
    if($con->query($sql)){
        echo "<script>alert('Registro Eliminado')</script>";
    }else{
        echo "<script>alert('No se pudo modificar....')</script>";
    }
    $con->close();
}

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css"
          integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">

    <link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap5.min.css">

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.min.js"
            integrity="sha384-ODmDIVzN+pFdexxHEHFBQH3/9/vQ9uori45z4JjnFsRydbmQbmL5t1tQ0culUzyK"
            crossorigin="anonymous"></script>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa"
            crossorigin="anonymous"></script>

    <script src="https://code.jquery.com/jquery-3.6.1.js"
            integrity="sha256-3zlB5s2uwoUzrXK3BT7AX3FyvojsraNFxCc2vC/7pNI=" crossorigin="anonymous"></script>

    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.4.29/dist/sweetalert2.all.min.js"
            integrity="sha256-qxQs7IPMvfbL9ZXhsZ3tG/LuZSjNxPSTBNtzk4j5FiU=" crossorigin="anonymous"></script>

    <script src="https://code.jquery.com/jquery-3.6.1.js"
            integrity="sha256-3zlB5s2uwoUzrXK3BT7AX3FyvojsraNFxCc2vC/7pNI=" crossorigin="anonymous"></script>

    <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>

    <script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap5.min.js"></script>
    <title>CRUD</title>
</head>
<body>
<div class="container">
        <!-- Button trigger modal -->
        <button type="button" id="btnModal" class="btn btn-primary mt-5" data-bs-toggle="modal" data-bs-target="#exampleModal">
            Nuevo
        </button>

        <hr>
    <div class="row text-center mx-auto">
        <div class="col-md-10">
            <div id="">
                <?= getProductos() ?>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="Productos.php" method="post" id="form">
                    <div class="form-floating mb-3">
                        <input type="text" class="form-control" id="txtCodigo" name="txtCodigo" placeholder="Codigo" required>
                        <label for="txtCodigo">Codigo</label>
                    </div>
                    <div class="form-floating mb-3">
                        <input type="text" class="form-control" id="txtNombre" name="txtNombre" placeholder="Nombre" required>
                        <label for="txtNombre">Nombre</label>
                    </div>
                    <div class="form-floating mb-3">
                        <input type="number" class="form-control" id="txtStock" name="txtStock" placeholder="Stock" required min="1">
                        <label for="txtStock">Stock</label>
                    </div>
                    <div class="form-floating mb-3">
                        <input type="number" class="form-control" id="txtCosto" name="txtCosto" placeholder="Costo" required min="1" step="0.1">
                        <label for="txtCosto">Costo</label>
                    </div>
                    <input type="submit" class="btn btn-primary" id="btnGuardar" name="btnGuardar" value="Guardar"/>
                    <input type="submit" class="btn btn-primary" id="btnEditar" name="btnEditar" value="Editar"/>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <!--<button type="button" class="btn btn-primary">Save changes</button>!-->
            </div>
        </div>
    </div>
</div>
<script>
    function cargarMo(codigo, nombre, stock, costo){
        // Cargar el formulario
        $("#txtCodigo").val(codigo);
        $("#txtNombre").val(nombre);
        $("#txtStock").val(stock);
        $("#txtCosto").val(costo);
        $("#btnGuardar").hide();
        $("#btnEditar").show();
        $("#txtCodigo").prop( "readonly", true );
        $('#exampleModalLabel').text('Modificar Producto');

    }

    function eliminar(codigo){
        $("#txtCodigo").val(codigo);
        Swal.fire({
            title: 'Esta seguro que desea eliminar?',
            text: "Esta accion no se puede revertir",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, Eliminar'
        }).then((result) => {
            if (result.isConfirmed) {
                $('#form').append($('<input></input>').attr('id','btnEliminar').attr('type','hidden').attr('name','btnEliminar').attr('value','btnEliminar'));
                $('#form').submit();
            }
        })
    }

    $(document).ready(function () {
        $('#resultado').DataTable();

        $("#btnModal").on("click", function(){
            $("#btnEditar").hide();
            $("#btnGuardar").show();
            $("#txtCodigo").prop( "readonly", false );
            $('#exampleModalLabel').text('Agregar Producto');
            $("#form")[0].reset();  //limpiar el form
        })
    });
</script>
</body>
</html>