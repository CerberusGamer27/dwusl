<?php
require_once 'vendor/autoload.php';
class Reporte{
    function getReporte(){
        $tabla = "<table>
    <thead>
    <tr>
        <th>Encabezado 1</th> <th>Encabezado 2</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>Cuerpo </td><td>Cuerpo </td>
    </tr>
    <tr>
        <td>Cuerpo </td><td>Cuerpo </td>
    </tr>
    <tr>
        <td>Cuerpo </td><td>Cuerpo </td>
    </tr>
    <tr>
        <td>Cuerpo </td><td>Cuerpo </td>
    </tr>
    </tbody>
</table>";
        $estilo = "<style>
    body {
        font-family: Arial, Helvetica, sans-serif;
    }
    table {
        font-family: \"Lucida Sans Unicode\", \"Lucida Grande\", Sans-Serif;
        font-size: 12px;
        margin: auto;
        width: 480px;
        text-align: left;
        border-collapse: collapse;
    }
    th {
        font-size: 13px;
        font-weight: normal;
        padding: 8px;
        background: #b9c9fe;
        border-top: 4px solid #aabcfe;
        border-bottom: 1px solid #fff;
        color: #039;
    }
    td {
        padding: 8px;
        background: #e8edff;
        border-bottom: 1px solid #fff;
        color: #669;
        border-top: 1px solid transparent;
    }
</style>";
        $img = "<img src=\"logo.png\" width=\"300\" height=\"106\">";
        $fecha = date("D j M Y G:i:s");

//crear un objeto de tipo mpdf
        $rpt= new  \Mpdf\Mpdf();
        $rpt->WriteHTML("<h2>Titulo del Documento</h2>");
        $rpt->WriteHTML($img);
        $rpt->WriteHTML("<hr>");
        $rpt->WriteHTML("Fecha del reporte " . $fecha);
        $rpt->WriteHTML("<h4 align='center'>Contenido de tabla</h4>");
        $rpt->WriteHTML($estilo);
        $rpt->WriteHTML($tabla);
        $rpt->SetHTMLFooter('
                <table>
                    <tr>
                        <td width="33%">{DATE j-m-Y}</td>
                        <td width="33%">{PAGENO}/{nbpg}</td>
                        <td width="33%">Copyright DS39-A</td>
                    </tr>
                </table>
        ');
        $nombreReporte = "Reporte2.pdf";
        $rpt->Output( $nombreReporte);
        return  $nombreReporte;
    }
}

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Reporte 2</title>
</head>
<body>
<h2>Reporte incrustado</h2>
<hr>
<form action="Reporte2.php" method="post">
    <input type="submit" value="Generar Reporte" name="btnReporte">
</form>
<?php
if(isset($_POST["btnReporte"])) {
    $rpt = new Reporte();
    $reporte = $rpt->getReporte();
    ?>
    <div style="margin: auto;">
        <iframe src="<?=$reporte ?>" style="width: 100%; height: 600px;" scrolling="yes">
        </iframe>
    </div>
    <?php
}
?>

</body>
</html>



