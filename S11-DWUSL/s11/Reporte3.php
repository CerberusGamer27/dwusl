<?php

use Mpdf\Mpdf;

require_once 'vendor/autoload.php';

class Rpt
{
    function getProdXProveedor($idProveedor)
    {
        $sql = "select * from producto where id_proveedor=" . $idProveedor;
        $con = new mysqli("localhost", "root", "", "dwusl_db");
        $html = "<table><thead><tr><th>CODIGO</th><th>NOMBRE</th><th>PRECIO COMPRA</th><th>STOCK</th><th>PRECIO VENTA</th></tr></thead><tbody>";
        try {
            $res = $con->query($sql);
            while ($fila = mysqli_fetch_assoc($res)) {
                $html .= "<tr>";
                $html .= "<td>" . $fila["codigo"] . "</td>";
                $html .= "<td>" . $fila["nombre"] . "</td>";
                $html .= "<td>" . $fila["precioCompra"] . "</td>";
                $html .= "<td>" . $fila["stock"] . "</td>";
                $html .= "<td>" . $fila["precioVenta"] . "</td>";
                $html .= "</tr>";
            }
            $html .= "</tbody></table>";
            $res->close();
            $con->close();

            //armamos el pdf
            $estilo = "<style>
    body {
        font-family: Arial, Helvetica, sans-serif;
    }
    table {
        font-family: \"Lucida Sans Unicode\", \"Lucida Grande\", Sans-Serif;
        font-size: 12px;
        margin: auto;
        width: 480px;
        text-align: left;
        border-collapse: collapse;
    }
    th {
        font-size: 13px;
        font-weight: normal;
        padding: 8px;
        background: #b9c9fe;
        border-top: 4px solid #aabcfe;
        border-bottom: 1px solid #fff;
        color: #039;
    }
    td {
        padding: 8px;
        background: #e8edff;
        border-bottom: 1px solid #fff;
        color: #669;
        border-top: 1px solid transparent;
    }
</style>";
            $pie = " <table>
                    <tr>
                        <td width=\"33%\">{DATE j-m-Y}</td>
                        <td width=\"33%\">{PAGENO}/{nbpg}</td>
                        <td width=\"33%\">Copyright DS39-A</td>
                    </tr>
                </table>";

            $mpdf = new Mpdf();
            $mpdf->SetTitle('<h1>Reporte de productos por proveedor</h1><hr>');
            $mpdf->WriteHTML("<h2 text-align='center'>Productos de proveedor $idProveedor </h2>");
            $mpdf->WriteHTML($estilo);
            $mpdf->WriteHTML($html);
            $nombreReporte = "Reporte3.pdf";
            $mpdf->SetHTMLFooter($pie);
            $mpdf->Output($nombreReporte);
            return $nombreReporte;

        } catch (Exception $ex) {
            echo "<script>console.log('" . $ex->getMessage() . "');</script>";
            return null;
        }
    }
}

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<h2>Reporte de productos por proveedor</h2>
<hr>
<form style="margin: auto;" method="post" action="Reporte3.php">
    Ver productos segun proveedor:
    <select name="cmbProveedor">
        <option value="1">Proveedor 1</option>
        <option value="2">Proveedor 2</option>
    </select>
    <input type="submit" value="Generar Reporte" name="btnRpt">
</form>
<?php
if (isset($_POST["btnRpt"])) {
    $id = $_POST["cmbProveedor"];
    $obj = new Rpt();
    $reporte = $obj->getProdXProveedor($id);
    if ($reporte == null) {
        echo "el reporte no se pudo cargar..";
    } else {

        ?>
        <iframe src="<?= $reporte ?>"
                style="width: 100%; height: 800px;" scrolling="yes">
        </iframe>

        <?php

    }
}


?>

</body>
</html>
