<?php

use Mpdf\Mpdf;

require_once 'vendor/autoload.php';
//preparar contenido para el documento
$tabla = "<table>
    <thead>
    <tr>
        <th>Encabezado 1</th> <th>Encabezado 2</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>Cuerpo </td><td>Cuerpo </td>
    </tr>
    <tr>
        <td>Cuerpo </td><td>Cuerpo </td>
    </tr>
    <tr>
        <td>Cuerpo </td><td>Cuerpo </td>
    </tr>
    <tr>
        <td>Cuerpos </td><td>Cuerpo </td>
    </tr>
    </tbody>
</table>";
$estilo = "<style>
    body {
        font-family: Arial, Helvetica, sans-serif;
    }
    table {
        font-family: \"Lucida Sans Unicode\", \"Lucida Grande\", Sans-Serif;
        font-size: 12px;
        margin: auto;
        width: 480px;
        text-align: left;
        border-collapse: collapse;
    }
    th {
        font-size: 13px;
        font-weight: normal;
        padding: 8px;
        background: #b9c9fe;
        border-top: 4px solid #aabcfe;
        border-bottom: 1px solid #fff;
        color: #039;
    }
    td {
        padding: 8px;
        background: #e8edff;
        border-bottom: 1px solid #fff;
        color: #669;
        border-top: 1px solid transparent;
    }
</style>";
$img = "<img src=\"logo.png\" width=\"300\" height=\"106\">";
//crear un objeto de tipo mpdf
$rpt= new  Mpdf();
$rpt->WriteHTML("<h2>Titulo del Documento</h2>");
$rpt->WriteHTML($img);
$rpt->WriteHTML("<hr>");
$rpt->WriteHTML("Aqui ponemos el texto que <b>Querramos </b>");
$rpt->WriteHTML("<h4 align='center'>Contenido de tabla</h4>");
$rpt->WriteHTML($estilo);
$rpt->WriteHTML($tabla);
$rpt->WriteHTML("<div style='margin-top: 80%;'><hr>N° de pág: {PAGENO}</div>");

$rpt->Output("Reporte1.pdf",'I');
exit;
?>




