<?php
$miArray = array('Item',2,3.14,true);
$miArray[]=1000;
$miArray[]="Ultimo Valor";
//var_dump($miArray);
// recorriendo con bucle for
for ($i=0; $i<sizeof($miArray); $i++){
    echo $miArray[$i]."<br>";
}

// Arrays Asociativos
$asoc = array("nombre"=>"Juan", "apellido" => "Perez","edad"=>20);
//var_dump($asoc);
echo "<br>";
//recorriendo arreglo asociativo
foreach ($asoc as $item){
    echo $item."<br>";
}

echo "<br>";

foreach ($asoc as $key=>$value){
    echo $key."=>".$value."<br>";
}
