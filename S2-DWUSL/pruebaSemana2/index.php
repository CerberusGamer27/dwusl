<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<h1>Welcome to the zone</h1>
<h4>Introduccion a PHP</h4>
<?php
// COMO INTRODUCIR CODIGO PHP EN UNA PAGINA
/*
*
* COMENTARIOS
* MULTILINEA
*/
$variable = 10;

echo "<h5>Codigo PHP embebido</h5> ";
$var0 = "<script>function msj(){alert('Hola Alerta');}</script>";
echo $var0;
$vari1 ="<input type='button' value='click me' onclick='msj()'>";
echo $vari1;

//Declaracion de variables

//TODAS CON SIGNO DE DOLAR
$nombre = "Juan Perez";
$edad = 10;
$num1 =10;
$num2 = 25;
$suma = $num1 + $num2;
echo "la suma es: " .$suma;

?>

<?= $variable?>
<select name="Departamentos">
    <option value="<?php echo $variable; ?>">Valor 1</option>
    <option value="<?= $variable?>">Valor 2</option>
    <option value="">Valor 3</option>
    <option value="">Valor 4</option>
    <option value="">Valor 5</option>
</select>
</body>
</html>
