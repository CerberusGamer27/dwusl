<?php
// ESTRUCTURAS DE PROGRAMACION
// SECUENCIALES
/*
 * crear un programa que permita calcular el salario final de un empleado en base
 * a su salario base si se sasbe que se descuenta un 10% de renta y un 7% de AFP
 * y 5% de iss
 *
*/
$salario = 1500;
$renta = $salario * 0.1;
$afp = $salario * 0.07;
$isss = $salario * 0.05;
$desc = $renta + $afp + $isss;
$sf = $salario - $desc;

echo "<script>alert('El salario final es: $sf')</script>";

// SELECTIVAS
/*
 * Si el salario final es menor a $500, se dara un bono de
 * $100, si el salario es entre $500.01 y $1000, el bono sera
 * de $50, si el salario es mayor a $1000.0 no hay bono
 */

if($sf <= 500){
    $sf += 100;
    echo "<script>alert('El salario final + bono es: $sf')</script>";
} else if($sf >=500.01 && $sf <=1000){
    $sf += 100;
    echo "<script>alert('El salario final + bono es: $sf')</script>";
} else {
    echo "<script>alert('No hay bono')</script>";
}

// SELECTIVA MULTIPLE
$dia = "viernes";
switch ($dia){
    case 'lunes':
        echo "Buen inicio de semana";
        break;
    case 'martes':
        echo "Segundo dia laboral";
        break;
    case 'miercoles':
        echo "Vamos a media semana animo";
        break;
    case 'jueves':
        echo "Ni idea";
        break;
    case 'viernes':
        echo "Es viernes y el cuerpo lo sabe";
        break;
}

// Estructuras de programacion iterativas (for finitas / infinitas while)

$num = 6;
echo "<br>";
for ($i = 1; $i<=10; $i++){
    echo "$num x $i = ".($num*$i). "<br>";
}
echo "<br>Usando while <br>";
$flag = 1;
while ($flag <=10){
    echo "$num x $flag = ".($num*$flag). "<br>";
    $flag++;
}
?>