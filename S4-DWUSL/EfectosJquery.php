<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.min.js" integrity="sha384-ODmDIVzN+pFdexxHEHFBQH3/9/vQ9uori45z4JjnFsRydbmQbmL5t1tQ0culUzyK" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
    <script>

        $(document).ready(function(){
            //code
            //seleccionando los controles
            let usr = $("#txtNombre")
            let pwd = $("#txtPwd")
            let pwd2 = $("#txtPwd2")
            let textoAyuda = $("#txtAyuda")
            let btnAyuda = $("#btnAyuda")
            let verPwd = $("#btnVerPwd1")
            let verPwd2 = $("#btnVerPwd2")

            //ocultando un control
            pwd2.hide()
            verPwd2.hide()
            textoAyuda.hide()

            usr.on("blur", function(){
                pwd2.show()
                verPwd2.show()
            });
            btnAyuda.on("click", function(){
                textoAyuda.toggle()
            });

            verPwd.on("click", function(){
                if(pwd.attr("type")=="password"){
                    pwd.attr("type","text")
                    verPwd.attr("value","Ocu")
                   // verPwd.removeClass("btn btn-default")
                   // verPwd.addClass("btn btn-danger")
                    verPwd.toggleClass("btn btn-default").toggleClass("btn btn-danger")

                }else{
                    pwd.attr("type","password")
                    verPwd.attr("value","Ver")
                    //verPwd.removeClass("btn btn-danger")
                   // verPwd.addClass("btn btn-default")
                    verPwd.toggleClass("btn btn-default").toggleClass("btn btn-danger")
                }

            });




        });



    </script>


    <title>Document</title>
</head>
<body><br>
<hr style="width: 600px; margin: auto">
<h4 align="center">Formulario de registro</h4>
<hr style="width: 600px; margin: auto">

<div style="width: 600px; margin: auto;"><br>
    <form class="form-group">
        <p id="txtAyuda" class="text small">
            Debe escribir su nombre completo, y la clave escribirla dos veces
        </p>
        <input type="text" class="form-control" placeholder="Nombre completo"
        id="txtNombre" style="float: left; width: 90%;" required>
        <input type="button" id="btnAyuda" value="?" class="btn btn-info" style="float: right">

        <input type="password" class="form-control" placeholder="Contraseña"
        id="txtPwd" style="float: left; width: 90%;">
        <input type="button" id="btnVerPwd1" value="Ver" class="btn btn-default" style="float: right">


        <input type="password" class="form-control" placeholder="Repita contraseña"
               id="txtPwd2" style="float: left; width: 90%;" >
        <input type="button" id="btnVerPwd2" value="Ver" class="btn btn-default" style="float: right">



        <input type="email" class="form-control" placeholder="Correo electronico"
               id="txtEmail" style="float: left; width: 90%;"><br>
        <input type="submit" name="btnRegistrar" value="Registrar" class="btn btn-success sm">
    </form>
</div>







</body>
</html>