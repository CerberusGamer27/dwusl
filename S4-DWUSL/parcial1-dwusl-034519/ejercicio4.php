<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Ejercicio 4 Parcial 1 - PHP</title>
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    <!-- JavaScript Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous">
    </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
</head>
<body>
<div class="container-md py-4">
    <header class="pb-3 mb-4 border-bottom">
        <a href="/" class="d-flex align-items-center text-dark text-decoration-none">
            <svg xmlns="http://www.w3.org/2000/svg" width="40" height="32" class="me-2" viewBox="0 0 118 94" role="img">
                <title>Bootstrap</title>
                <path fill-rule="evenodd" clip-rule="evenodd"
                      d="M24.509 0c-6.733 0-11.715 5.893-11.492 12.284.214 6.14-.064 14.092-2.066 20.577C8.943 39.365 5.547 43.485 0
                    44.014v5.972c5.547.529 8.943 4.649 10.951 11.153 2.002 6.485 2.28 14.437 2.066 20.577C12.794 88.106 17.776 94 24.51
                    94H93.5c6.733 0 11.714-5.893 11.491-12.284-.214-6.14.064-14.092 2.066-20.577 2.009-6.504 5.396-10.624
                    10.943-11.153v-5.972c-5.547-.529-8.934-4.649-10.943-11.153-2.002-6.484-2.28-14.437-2.066-20.577C105.214 5.894 100.233 0 93.5
                    0H24.508zM80 57.863C80 66.663 73.436 72 62.543 72H44a2 2 0 01-2-2V24a2 2 0 012-2h18.437c9.083 0 15.044 4.92 15.044 12.474 0
                    5.302-4.01 10.049-9.119 10.88v.277C75.317 46.394 80 51.21 80 57.863zM60.521 28.34H49.948v14.934h8.905c6.884 0 10.68-2.772
                    10.68-7.727 0-4.643-3.264-7.207-9.012-7.207zM49.948 49.2v16.458H60.91c7.167 0 10.964-2.876 10.964-8.281
                    0-5.406-3.903-8.178-11.425-8.178H49.948z"
                      fill="currentColor"></path>
            </svg>
            <span class="fs-1">Ejercicio 4 DWUSL - 034519</span>
        </a>
    </header>
    <main>
        <div class="row">
            <div class="col-md-5 col-lg-4">
                <form id="form1" class="form-group" method="post" action="Ejercicio4.php">
                    <h4 class="text-center">Boleta de Pago</h4>
                    <div class="form-floating mb-3">
                        <input type="text" name="txtNombre" id="txtNombre" class="form-control"
                               placeholder="Nombre de empleado" required>
                        <label for="txtNombre">Nombre de empleado</label>
                    </div>
                    <div class="form-floating mb-3">
                        <input type="text" name="txtCargo" id="txtCargo" class="form-control" placeholder="Cargo">
                        <label for="txtCargo">Cargo</label>
                    </div>
                    <div class="form-floating mb-3">
                        <input type="number" name="txtAnti" id="txtAnti" class="form-control" min="0" value="1"
                               placeholder="Antiguedad"
                               required>
                        <label for="txtAnti">Antiguedad</label>
                    </div>
                    <div class="form-floating mb-3">
                        <select class="form-select" id="cmbTipo" name="cmbTipo">
                            <option value="">Seleccione...</option>
                            <option value="Planilla">Planilla</option>
                            <option value="Servicio profesional">Servicio profesional</option>
                        </select>
                        <label for="cmbTipo">Tipo de Empleado</label>
                    </div>
                    <label for="">Destacado</label>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="rDestacado" value="Si"
                               id="rDestacadoSi">
                        <label class="form-check-label" for="rDestacadoSi">
                            Sí
                        </label>
                    </div>
                    <div class="form-check mb-3">
                        <input class="form-check-input" type="radio" name="rDestacado" value="No"
                               id="rDestacadoNo" checked>
                        <label class="form-check-label" for="rDestacadoNo">
                            No
                        </label>
                    </div>
                    <div class="form-floating mb-3">
                        <input type="text" name="txtSalario" id="txtSalario" class="form-control" value="0"
                               placeholder="Salario Base">
                        <label for="txtSalario">Salario Base</label>
                    </div>
                    <div class="form-floating mb-3">
                        <input type="email" name="txtEmail" id="txtEmail" class="form-control"
                               placeholder="Correo Electrónico">
                        <label for="txtEmail">Correo Electrónico</label>
                    </div>
                    <input type="submit" name="btnEnviar" class="btn btn-outline-primary" value="Enviar">
                </form>
            </div>
            <div class="col-md-7 col-lg-8 order-md-last">
                <?php
                if (isset($_REQUEST["btnEnviar"])) {
                    $nombre = $_REQUEST["txtNombre"];
                    $cargo = $_REQUEST["txtCargo"];
                    $antiguedad = $_REQUEST["txtAnti"];
                    $tipo = $_REQUEST["cmbTipo"];
                    $destacado = $_REQUEST["rDestacado"];
                    $salarioBase = $_REQUEST["txtSalario"];
                    $email = $_REQUEST["txtEmail"];
                    $bonus = 50;
                    $porcentaje = 0.05;
                    $isss = 0.03;
                    $afp = 0.0725;
                    $isr = 0.1;
                    if ($destacado == "Si") {
                        $bonus = $bonus + $salarioBase;
                    }

                    if ($antiguedad > 2) {
                        $porcentaje = $salarioBase * $porcentaje;
                    }
                    $isss = $salarioBase * $isss;
                    $afp = $salarioBase * $afp;
                    $isr = $salarioBase * $isr;
                    $descuentos = $isss + $afp + $isr;
                    $salarioFinal = $salarioBase - $descuentos;
                    $salarioFinal = $salarioFinal + $bonus + $porcentaje;


                    ?>
                    <table class="table">
                        <thead>
                            <th>Informacion Empleado</th>
                            <th>Datos</th>
                        </thead>
                        <tbody>
                        <tr>
                            <td>Nombre</td>
                            <td><?= $nombre ?> </td>
                        </tr>
                        <tr>
                            <td>Cargo</td>
                            <td><?= $cargo ?> </td>
                        </tr>
                        <tr>
                            <td>Antiguedad</td>
                            <td><?= $antiguedad ?> </td>
                        </tr>
                        <tr>
                            <td>Tipo de empleado</td>
                            <td><?= $tipo ?> </td>
                        </tr>
                        <tr>
                            <td>Destacado</td>
                            <td><?= $destacado ?> </td>
                        </tr>
                        <tr>
                            <td>Salario Base</td>
                            <td><?= $salarioBase ?> </td>
                        </tr>
                        <tr>
                            <td>Descuentos</td>
                            <td><?= $descuentos ?> </td>
                        </tr>
                        <tr>
                            <td>Salario Final</td>
                            <td><?= $salarioFinal ?> </td>
                        </tr>
                        </tbody>
                    </table>

                    <?php
                }
                ?>
            </div>
        </div>
    </main>
    <script>

    </script>
</div>
</body>
</html>