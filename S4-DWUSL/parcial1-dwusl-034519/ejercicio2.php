<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Ejercicio 3 Parcial 1 - PHP</title>
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    <!-- JavaScript Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous">
    </script>
</head>
<body>
<div class="container-md py-4">
    <header class="pb-3 mb-4 border-bottom">
        <a href="/" class="d-flex align-items-center text-dark text-decoration-none">
            <svg xmlns="http://www.w3.org/2000/svg" width="40" height="32" class="me-2" viewBox="0 0 118 94" role="img">
                <title>Bootstrap</title>
                <path fill-rule="evenodd" clip-rule="evenodd"
                      d="M24.509 0c-6.733 0-11.715 5.893-11.492 12.284.214 6.14-.064 14.092-2.066 20.577C8.943 39.365 5.547 43.485 0
                    44.014v5.972c5.547.529 8.943 4.649 10.951 11.153 2.002 6.485 2.28 14.437 2.066 20.577C12.794 88.106 17.776 94 24.51
                    94H93.5c6.733 0 11.714-5.893 11.491-12.284-.214-6.14.064-14.092 2.066-20.577 2.009-6.504 5.396-10.624
                    10.943-11.153v-5.972c-5.547-.529-8.934-4.649-10.943-11.153-2.002-6.484-2.28-14.437-2.066-20.577C105.214 5.894 100.233 0 93.5
                    0H24.508zM80 57.863C80 66.663 73.436 72 62.543 72H44a2 2 0 01-2-2V24a2 2 0 012-2h18.437c9.083 0 15.044 4.92 15.044 12.474 0
                    5.302-4.01 10.049-9.119 10.88v.277C75.317 46.394 80 51.21 80 57.863zM60.521 28.34H49.948v14.934h8.905c6.884 0 10.68-2.772
                    10.68-7.727 0-4.643-3.264-7.207-9.012-7.207zM49.948 49.2v16.458H60.91c7.167 0 10.964-2.876 10.964-8.281
                    0-5.406-3.903-8.178-11.425-8.178H49.948z"
                      fill="currentColor"></path>
            </svg>
            <span class="fs-1">Ejercicio 3 DWUSL - 034519</span>
        </a>
    </header>
    <?php
    ?>
    <main>
        <h4>Datos</h4>
        <div class="row">
            <div class="col-10 text-center">
                <form method="post" action="ejercicio2.php">
                    <div class="row mb-3">
                        <label for="numeroTabla" class="col-sm-2 col-form-label">Tabla Multiplicar</label>
                        <div class="col-sm-10">
                            <input type="number" max="10" min="1" class="form-control" id="numeroTabla" required
                                   name="numeroTabla" title="Ingrese un numero entre 1 y 10">
                        </div>
                    </div>
                    <div class="row mb-3">
                            <?php
                            if (isset($_REQUEST["btnEnviar"])){
                                $valor= $_REQUEST["numeroTabla"];

                                $valores = [];
                                for ($i = 1; $i <=10; $i++){
                                    $valores[$i] = $i*$valor;
                                }
                                //var_dump($valores);
                            ?>
                        <label for="" class="col-sm-2 col-form-label">Valores</label>
                        <div class="col-sm-10">
                            <select class="form-select" aria-label="Default select example">
                                <?php foreach ($valores as $numero): ?>
                                    <option value="<?= $numero ?>"><?= $numero ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                            <?php
                                }
                            ?>
                    </div>
                    <button type="submit" id="btnEnviar" name="btnEnviar" class="btn btn-primary">Enviar</button>
                </form>
            </div
        </div>
    </main>
</div>
</body>
</html>