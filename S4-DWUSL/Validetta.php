<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.min.js" integrity="sha384-ODmDIVzN+pFdexxHEHFBQH3/9/vQ9uori45z4JjnFsRydbmQbmL5t1tQ0culUzyK" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>

    <!-- incorporacion de validetta -->
    <link rel="stylesheet" href="validetta/dist/validetta.min.css">
    <script src="validetta/dist/validetta.min.js"></script>
    <script src="validetta/localization/validettaLang-es-ES.js"></script>

    <title>Document</title>
</head>
<body>


<div style="width: 600px; margin: auto;"><br>
    <form class="form-group" method="post" action="Validetta.php" id="formulario1">
        <input type="text" class="form-control" placeholder="Nombre completo"
               id="txtNombre" data-validetta="required">

        <input type="password" class="form-control" placeholder="Contraseña"
               id="txtPwd" data-validetta="required">

        <input type="password" class="form-control" placeholder="Repita contraseña"
               id="txtPwd2" data-validetta="equalTo[txtPwd]" >

        <input type="email" class="form-control" placeholder="Correo electronico"
               id="txtEmail" data-validetta="email"><br>

        <input type="submit" name="btnRegistrar" value="Registrar" class="btn btn-success sm">
    </form>
</div>
<script>
    $(document).ready(function(){
        //algo que es necesario para usar validetta
        $("#formulario1").validetta({
            showErrorMessages : true,
            display : 'bubble',
            realTime: true
        });
    });
</script>
</body>
</html>