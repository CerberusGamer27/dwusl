<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css"
          integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.min.js"
            integrity="sha384-ODmDIVzN+pFdexxHEHFBQH3/9/vQ9uori45z4JjnFsRydbmQbmL5t1tQ0culUzyK"
            crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa"
            crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.js"
            integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="resources/validetta/dist/validetta.min.css" type="text/css" media="screen">
    <script src="resources/validetta/dist/validetta.min.js"></script>
    <script src="resources/validetta/localization/validettaLang-es-ES.js"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <script>
        $(document).ready(function () {
            $("#form").validetta({
                realTime: true,
                display: 'inline',
                validators: {
                    regExp: {
                        regSalario: {
                            pattern: /(25[0-9]|2[6-9][0-9]|[3-6][0-9]{2}|700)/,
                            errorMessage: 'El salario debe estar entre $250 y $700'
                        },
                        regAnios: {
                            pattern: /^[1-9][0-9]*$/,
                            errorMessage: 'La antiguedad debe ser un numero entero mayor a 0'
                        }
                    }
                },
            });
        });
    </script>

    <title>Ejercicio 4</title>
</head>
<body>
<br>
<div class="container">
    <h1>Formulario Empleado</h1>
    <br>
    <form id="form">
        <div class="row">
            <div class="col-6 mb-3">
                <label for="txtnNombre">Nombre Completo</label>
                <input data-validetta="required" name="txtNombre" id="txtNombre" type="text" class="form-control"
                       placeholder="Nombre Completo">
            </div>
            <div class="col-4 mb-3">
                <label for="txtCargo">Cargo</label>
                <input data-validetta="required" name="txtCargo" id="txtCargo" type="text" class="form-control"
                       placeholder="Cargo">
            </div>
            <div class="col-2 mb-3">
                <label for="txtAntiguedad">Antiguedad</label>
                <input data-validetta="required,number,regExp[regAnios]" name="txtAntiguedad" id="txtAntiguedad"
                       type="number" class="form-control" placeholder="Años">
            </div>
        </div>
        <div class="row">
            <div class="col-8 mb-3">
                <label for="slTipoEmpleado">Tipo de empleado</label>
                <select name="slTipoEmpleado" id="slTipoEmpleado" class="form-select">
                    <option selected value="Planilla">Planilla</option>
                    <option value="Servicios Profesionales">Servicios Profesionales</option>
                </select>
            </div>
            <div class="col-2 mb-3">
                <label for="slDestacado">Destacado</label>
                <select name="slDestacado" id="slDestcado" class="form-select">
                    <option selected value="SI">SI</option>
                    <option value="NO">NO</option>
                </select>
            </div>

            <div class="col-2 mb-3">
                <label for="txtSalario">Salario</label>
                <input data-validetta="required,number,regExp[regSalario]" name="txtSalario" id="txtSalario" type="text"
                       class="form-control" placeholder="$0.00">
            </div>
        </div>
        <div class="row">
            <div class="col-12 mb-3">
                <label for="txtCorreo">Correo Electronico</label>
                <input data-validetta="required,email" name="txtCorreo" id="txtCorreo" type="text" class="form-control"
                       placeholder="example@example.com">
            </div>
        </div>
        <button name="btnProcesar" type="submit" class="btn btn-primary">Procesar</button>
    </form>
</div>
</body>
</html>

<?php
if (isset($_REQUEST["btnProcesar"])) {
    $nombre = $_REQUEST["txtNombre"];
    $cargo = $_REQUEST["txtCargo"];
    $antiguedad = $_REQUEST["txtAntiguedad"];
    $tipoEmpleado = $_REQUEST["slTipoEmpleado"];
    $destacado = $_REQUEST["slDestacado"];
    $salario = $_REQUEST["txtSalario"];
    $correo = $_REQUEST["txtCorreo"];
    $isss = $salario*0.03;
    $afp = $salario*0.0725;
    $isr = $salario*0.1;
    $totalDescuentos = $isss+$afp+$isr;
    $salarioConDescuentos = $salario-$totalDescuentos;
    $salarioFinal = $salarioConDescuentos;
    $bonoDestacado =0;
    $bonoAntiguedad = 0;

    if($destacado == "SI"){
        $bonoDestacado =50;
        $salarioFinal+=50;
    }
    if($antiguedad > 2){
        $bonoAntiguedad = $salario*0.05;
        $salarioFinal+=($salario*0.05);
    }

    echo "<script>
            Swal.fire({
                title:'$nombre',
                html: 'Cargo: $cargo <br> Antiguedad: $antiguedad Años' +
                 '<br>Tipo Empleado: $tipoEmpleado <br> Destacado: $destacado' +
                  '<br>Salario Base: $salario <br> ISS: $$isss <br> AFP: $$afp <br> ISR: $$isr' +
                   '<br>Salario con descuentos: $$salarioConDescuentos' +
                    '<br>Bono por destacado: $$bonoDestacado <br>Bono por antiguedad: $$bonoAntiguedad' +
                   '<br>Total a pagar: <strong>$$salarioFinal</strong>'
            });
          </script>";
}
?>