<?php
class Categoria{
    private $idCategoria;
    private $nombreCat;
    private $descripcion;

    /**
     * @return mixed
     */
    public function getIdCategoria()
    {
        return $this->idCategoria;
    }

    /**
     * @param mixed $idCategoria
     * @return Categoria
     */
    public function setIdCategoria($idCategoria)
    {
        $this->idCategoria = $idCategoria;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNombreCat()
    {
        return $this->nombreCat;
    }

    /**
     * @param mixed $nombreCat
     * @return Categoria
     */
    public function setNombreCat($nombreCat)
    {
        $this->nombreCat = $nombreCat;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * @param mixed $descripcion
     * @return Categoria
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
        return $this;
    }


}
?>