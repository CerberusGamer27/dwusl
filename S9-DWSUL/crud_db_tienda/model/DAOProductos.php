<?php
require_once 'config.php';
require_once 'Productos.php';

class DAOProductos{
    private $con;

    private function conectar(){
        try{
            $this->con = new mysqli(SERVER,
                USER,
                PASS,
                DB);
        }catch(Exception $ex){
                //CODE
        }
    }

    private function desconectar(){
        $this->con->close();
    }

    public function get(){
        //EJEMPLO DE CONSULTAS NO PREPARADAS
        $sql = "select a.codigo, a.nombre, a.stock, a.costo, b.nombrecat as categoria from productos a ";
        $sql .= "inner join categoria b on a.idcategoria = b.idcategoria";
        $this->conectar();
        $html="<table class='table table-hover table-sm' id='tabla'><thead><th>CODIGO</th><th>NOMBRE</th><th>STOCK</th><th>COSTO</th><th>CATEGORIA</th><th>ACCION</th></thead><tbody>";
        try{
            $res = $this->con->query($sql);
            while($fila = mysqli_fetch_assoc($res)){
                $html .= "<tr>";
                $html .= "<td>".$fila["codigo"]."</td>";
                $html .= "<td>".$fila["nombre"]."</td>";
                $html .= "<td>".$fila["stock"]."</td>";
                $html .= "<td>".$fila["costo"]."</td>";
                $html .= "<td>".$fila["categoria"]."</td>";
                $html .= "<td><a class=\"btn btn-danger \" href=\"javascript:eliminar('".$fila["codigo"]."')\"><i class=\"bi bi-cart-x\"></i></a>";
                $html .= "<a class=\"btn btn-warning \" href=\"javascript:editar('".$fila["codigo"]."')\"><i class=\"bi bi-pencil-square\"></i></a></td>";
                $html .= "</tr>";
            }
            $html.= "</tbody></table>";
            $res->close();
            $this->desconectar();
        }catch (Exception $ex){
            echo "<script>console.log('".$ex->getMessage()."');</script>";
        }
        return $html;
    }

    public function fillData($id){
        //EJEMPLO DE CONSULTAS NO PREPARADAS
        $sql = "select a.codigo, a.nombre, a.stock, a.costo, a.idcategoria as categoria from productos a ";
        $sql .= "inner join categoria b on a.idcategoria = b.idcategoria where a.codigo = $id";
        $this->conectar();
        $dataArray = array();
        try{
            $res = $this->con->query($sql);
            while($fila = mysqli_fetch_assoc($res)){
                $dataArray[] = $fila;
            }
            $res->close();
            $this->desconectar();
        }catch (Exception $ex){
            http_response_code(400);
            header("Content-Type: application/json");
            return json_encode($ex->getMessage());
        }
        header("Content-Type: application/json");
        return json_encode($dataArray);
    }

    public function getCategorias(){
        //CONSULTA NO PREPARADA
        $sql = "select idcategoria, nombrecat from categoria";
        $html="";
        try{
            $this->conectar();
            $res = $this->con->query($sql);
            $html .= "<option selected disabled>Seleccione ...</option>";
            while($fila = mysqli_fetch_assoc($res)){
                $html.= "<option value='".$fila["idcategoria"]."'>".$fila["nombrecat"]."</option>";
            }
            $this->desconectar();
            $res->close();
        }catch (Exception $ex){
            http_response_code(400);
            header("Content-Type: application/json");
            return json_encode($ex->getMessage());
        }
        return $html;
    }

    public function insertar(Productos $prod){
        //USANDO CONSULTAS PREPARADAS
            try{
                $this->conectar();
                //crear la sentencia preparada
                $pstm = $this->con->prepare("insert into productos(nombre, stock, costo, idcategoria) values(?,?,?,?)");
                //establecer parametros al PreparedStatement
                $nombre = $prod->getNombre();
                $stock = $prod->getStock();
                $costo = $prod->getCosto();
                $idcategoria = $prod->getIdcategoria();
                //incluirlos en la consulta (establecer parametros)
                //s = string
                // i = integer
                // d = digit   (12.365)
                // b = boolean    true, false
                $pstm->bind_param("sidi",
                    $nombre,
                    $stock,
                    $costo,
                    $idcategoria);

                //ejecutar la sentencia
                if($pstm->execute()){
                    $pstm->close();
                    $this->desconectar();
                    return true;
                }else{
                    $pstm->close();
                    $this->desconectar();
                    return false;
                }

            }catch(Exception $ex){
                         ///code
            }
    }


    public function eliminar($codigo){
        //USANDO CONSULTA PREPARADA
        $this->conectar();
        $pstm = $this->con->prepare("delete from productos where codigo=?");
        $pstm->bind_param("i",$codigo);
        if($pstm->execute()){
            $pstm->close();
            $this->desconectar();
            return true;
        }else{
            $pstm->close();
            $this->desconectar();
            return false;
        }
    }

    public function modificar(Productos $prod){
        //USANDO CONSULTAS PREPARADAS
        try{
            $this->conectar();
            //crear la sentencia preparada
            $pstm = $this->con->prepare("update productos set nombre = ?, stock = ?, costo = ?, idcategoria = ? where codigo = ?");
            //establecer parametros al PreparedStatement
            $codigo = $prod->getCodigo();
            $nombre = $prod->getNombre();
            $stock = $prod->getStock();
            $costo = $prod->getCosto();
            $idcategoria = $prod->getIdcategoria();
            //incluirlos en la consulta (establecer parametros)
            // s = string
            // i = integer
            // d = digit   (12.365)
            // b = boolean    true, false
            $pstm->bind_param("sidii",
                $nombre,
                $stock,
                $costo,
                $idcategoria,
                $codigo);

            //ejecutar la sentencia
            if($pstm->execute()){
                $pstm->close();
                $this->desconectar();
                return true;
            }else{
                $pstm->close();
                $this->desconectar();
                return false;
            }

        }catch(Exception $ex){
            ///code
        }
    }
}

?>
