<?php
require_once 'config.php';
require_once 'Categoria.php';

class DAOCategoria
{
    private $con;

    private function conectar()
    {
        try {
            $this->con = new mysqli(SERVER,
                USER,
                PASS,
                DB);
        } catch (Exception $ex) {
            //CODE
        }
    }

    private function desconectar()
    {
        $this->con->close();
    }

    public function get()
    {
        //EJEMPLO DE CONSULTAS NO PREPARADAS
        $sql = "select idcategoria, nombrecat, descripcion from categoria;";
        $this->conectar();
        $html = "<table class='table table-hover table-sm' id='tabla'><thead><th>CODIGO</th><th>NOMBRE</th><th>DESCRIPCION</th><th>ACCIONES</th></thead><tbody>";
        try {
            $res = $this->con->query($sql);
            while ($fila = mysqli_fetch_assoc($res)) {
                $html .= "<tr>";
                $html .= "<td>" . $fila["idcategoria"] . "</td>";
                $html .= "<td>" . $fila["nombrecat"] . "</td>";
                $html .= "<td>" . $fila["descripcion"] . "</td>";
                $html .= "<td><a class=\"btn btn-danger \" href=\"javascript:eliminar('" . $fila["idcategoria"] . "')\"><i class=\"bi bi-cart-x\"></i></a>";
                $html .= "<a class=\"btn btn-warning \" href=\"javascript:editar('" . $fila["idcategoria"] . "')\"><i class=\"bi bi-pencil-square\"></i></a></td>";
                $html .= "</tr>";
            }
            $html .= "</tbody></table>";
            $res->close();
            $this->desconectar();
        } catch (Exception $ex) {
            echo "<script>console.log('" . $ex->getMessage() . "');</script>";
        }
        return $html;
    }

    public function insertar(Categoria $cat)
    {
        //USANDO CONSULTAS PREPARADAS
        try {
            $this->conectar();
            //crear la sentencia preparada
            $pstm = $this->con->prepare("insert into categoria(nombrecat, descripcion) values(?,?)");
            //establecer parametros al PreparedStatement
            $nombre = $cat->getNombrecat();
            $descripcion = $cat->getDescripcion();
            //incluirlos en la consulta (establecer parametros)
            //s = string
            // i = integer
            // d = digit   (12.365)
            // b = boolean    true, false
            $pstm->bind_param("ss",
                $nombre,
                $descripcion
            );

            //ejecutar la sentencia
            if ($pstm->execute()) {
                $pstm->close();
                $this->desconectar();
                return true;
            } else {
                $pstm->close();
                $this->desconectar();
                return false;
            }

        } catch (Exception $ex) {
            ///code
        }
    }

    public function eliminar($codigo)
    {
        //USANDO CONSULTA PREPARADA
        try {

            $this->conectar();
            $pstm = $this->con->prepare("delete from categoria where idcategoria=?");
            $pstm->bind_param("i", $codigo);
            if ($pstm->execute()) {
                $pstm->close();
                $this->desconectar();
                return true;
            } else {
                $pstm->close();
                $this->desconectar();
                $data = [ 'error' => 'La categoria tiene asociadas productos, no se puede eliminar'];
                http_response_code(400);
                header('Content-type: application/json');
                return json_encode( $data );
            }
        } catch (Exception $ex) {
            http_response_code(400);
            header("Content-Type: application/json");
            return json_encode($ex->getMessage());
        }
    }

    public function fillData($id)
    {
        //EJEMPLO DE CONSULTAS NO PREPARADAS
        $sql = "select idcategoria, nombrecat, descripcion from categoria where idcategoria = $id";
        $this->conectar();
        $dataArray = array();
        try {
            $res = $this->con->query($sql);
            while ($fila = mysqli_fetch_assoc($res)) {
                $dataArray[] = $fila;
            }
            $res->close();
            $this->desconectar();
        } catch (Exception $ex) {
            header("Content-Type: application/json");
            return json_encode($ex->getMessage());
        }
        header("Content-Type: application/json");
        return json_encode($dataArray);
    }

    public function modificar(Categoria $cat)
    {
        //USANDO CONSULTAS PREPARADAS
        try {
            $this->conectar();
            //crear la sentencia preparada
            $pstm = $this->con->prepare("update categoria set nombrecat = ?, descripcion = ? where idcategoria = ?");
            //establecer parametros al PreparedStatement
            $nombre = $cat->getNombrecat();
            $descripcion = $cat->getDescripcion();
            $idcategoria = $cat->getIdcategoria();
            //incluirlos en la consulta (establecer parametros)
            // s = string
            // i = integer
            // d = digit   (12.365)
            // b = boolean    true, false
            $pstm->bind_param("ssi",
                $nombre,
                $descripcion,
                $idcategoria);

            //ejecutar la sentencia
            if ($pstm->execute()) {
                $pstm->close();
                $this->desconectar();
                return true;
            } else {
                $pstm->close();
                $this->desconectar();
                return false;
            }

        } catch (Exception $ex) {
            ///code
        }
    }
}

?>
