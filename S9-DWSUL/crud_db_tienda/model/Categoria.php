<?php
class Categoria{
    private $idcategoria;
    private $nombrecat;
    private $descripcion;

    /**
     * @return mixed
     */
    public function getIdcategoria()
    {
        return $this->idcategoria;
    }

    /**
     * @param mixed $idcategoria
     * @return Categoria
     */
    public function setIdcategoria($idcategoria)
    {
        $this->idcategoria = $idcategoria;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNombrecat()
    {
        return $this->nombrecat;
    }

    /**
     * @param mixed $nombrecat
     * @return Categoria
     */
    public function setNombrecat($nombrecat)
    {
        $this->nombrecat = $nombrecat;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * @param mixed $descripcion
     * @return Categoria
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
        return $this;
    }


}

?>
