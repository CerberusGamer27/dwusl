function cargarTabla(){
    var div = $("#contenido");
    $.ajax({
        url:'../controller/CategoriasController.php',
        type:'post',
        data:{key:'get'}
    }).done(function(resp){
        div.empty();
        div.append(resp);
        //para formato de la tabla
        $("#tabla").DataTable({
            searching: true,
            language: {
                "info": "Mostrando _START_ a _END_ de _TOTAL_ Registros",
                "lengthMenu": "Mostrar _MENU_ registros",
                "Search" : "Buscar",
                "paginate": {
                    "first":      "Primero",
                    "last":       "Última",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                }
            },
            "order": [], //Initial no order.
        });

    }).fail(function(){
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Error al Cargar la Tabla',
        })
    });
}

function eliminar(codigo){
    Swal.fire({
        title: 'Esta seguro que desea eliminar el producto?',
        showDenyButton: true,
        confirmButtonText: 'Eliminar',
        denyButtonText: `Cancelar`,
    }).then((result) => {
        if (result.isConfirmed) {
            //codigo
            $.ajax({
                url:'../controller/CategoriasController.php',
                type:'post',
                data:{key:'eliminar',codigo:codigo}
            }).done(function(resp){
                if(resp){
                    Swal.fire({
                        icon: 'success',
                        title: 'Eliminado Correctamente',
                        showConfirmButton: false,
                        timer: 1500
                    })
                    cargarTabla();
                }else{
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'Error al Eliminar',
                    })
                }
            }).fail(function(resp){
                console.log(resp.responseJSON.error)
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: resp.responseJSON.error,
                })
            });
        } else if (result.isDenied) {
            Swal.fire('Producto no Eliminado', '', 'info')
        }
    })
}

async function editar(codigo){
    await $.ajax({
        url:'../controller/CategoriasController.php',
        type:'post',
        data:{key:'fillData',codigo:codigo}
    }).done(function(resp){
        if(resp){
            console.log(resp[0])
            $("#txtCodigo").val(resp[0].idcategoria).prop('readonly', true);
            $("#txtNombre").val(resp[0].nombrecat);
            $("#txtDescripcion").val(resp[0].descripcion);
            $('#exampleModal').modal('show');
            $("#btnGuardar").hide();
            $("#btnModificar").show();
        }
    }).fail(function(){
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Error al Insertar',
        })
    });
}


$(document).ready(function(){
    //cargar tabla html
    cargarTabla();

    //cargar select del modal
    $("#btnNuevo").on("click", function(){
        $("#txtCodigo").prop('readonly', false);
        $("#btnGuardar").show();
        $("#btnModificar").hide();
        $("#form1")[0].reset();  //limpiar el form
    })

    $("#btnGuardar").on("click", function(){
        var formulario = $("#form1").serialize();
        $.ajax({
            url:'../controller/CategoriasController.php',
            type:'post',
            data:{key:'insertar',data:formulario}
        }).done(function(resp){
            if(resp){
                Swal.fire({
                    position: 'top-end',
                    icon: 'success',
                    title: 'Guardado con Exito',
                    showConfirmButton: false,
                    timer: 1500
                })
                cargarTabla();
                $("#btnCerrarModal").click();
            }
        }).fail(function(){
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Error al Insertar',
            })
        });
    })

    $("#btnModificar").on("click", function(){
        var formulario = $("#form1").serialize();
        $.ajax({
            url:'../controller/CategoriasController.php',
            type:'post',
            data:{key:'modificar',data:formulario}
        }).done(function(resp){
            if(resp){
                Swal.fire({
                    position: 'top-end',
                    icon: 'success',
                    title: 'Guardado con Exito',
                    showConfirmButton: false,
                    timer: 1500
                })
                cargarTabla();
                $("#btnCerrarModal").click();
            }
        }).fail(function(){
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Error al Insertar',
            })
        });
    })

});