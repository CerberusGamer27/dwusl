<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- CDN'S -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css"
        integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.min.js"
            integrity="sha384-ODmDIVzN+pFdexxHEHFBQH3/9/vQ9uori45z4JjnFsRydbmQbmL5t1tQ0culUzyK"
            crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa"
            crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.6.1.js"
            integrity="sha256-3zlB5s2uwoUzrXK3BT7AX3FyvojsraNFxCc2vC/7pNI=" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.9.1/font/bootstrap-icons.css">
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.4.33/dist/sweetalert2.all.min.js"
            integrity="sha256-uGRHvDtVpBeFd7aKWnNdg7qIo+f+dQPlFRMSTqOq7o8=" crossorigin="anonymous"></script>

    <!-- para tabla paginada con filtros -->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap5.min.css">
    <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap5.min.js"></script>
    <script src="toolsCategorias.js"></script>


    <title>CRUD Productos</title>
</head>
<body>

<!-- -menu -->
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container-fluid">
        <a class="navbar-brand" href="#">EJEMPLO CLASE 9 DWUSL</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item">
                    <a class="nav-link" href="../index.php">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="Productos.php">Productos</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="Categorias.php">Categorias</a>
                </li>
            </ul>
        </div>
    </div>
</nav>


<br>
<h4 align="center">Soporte Tabla Categorias</h4>
<br>&nbsp;
<input type="button" id="btnNuevo" class="btn btn-success btn-sm"
    data-bs-toggle="modal" data-bs-target="#exampleModal" value="Nuevo Registro ">


<hr>

<div id="contenido" style="margin: auto; width: 60%;">


</div>

</body>
</html>

<!-- modal -->

<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">::Categorias::</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form id="form1">
                <div class="modal-body">
                    <div class="form-floating mb-3">
                        <input type="text" id="txtCodigo" name="txtCodigo"
                               placeholder="Codigo de Categoria" required class="form-control">
                        <label for="txtCodigo">Cod. Categoria</label>
                    </div>
                    <div class="form-floating mb-3">
                        <input type="text" id="txtNombre" name="txtNombre"
                               placeholder="Nombre de Categoria" required class="form-control">
                        <label for="txtNombre">Nombre Categoria</label>
                    </div>
                    <div class="form-floating mb-3">
                        <input type="text" id="txtDescripcion" name="txtDescripcion"
                               placeholder="Descripicion de Categoria" required class="form-control">
                        <label for="txtDescripcion">Descripcion Categoria</label>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" id="btnCerrarModal" class="btn btn-secondary btn-sm" data-bs-dismiss="modal">
                        Close
                    </button>
                    <input type="button" id="btnGuardar" name="btnGuardar" class="btn btn-primary btn-sm"
                           value="Guardar">
                    <input type="button" id="btnModificar" name="btnModificar" class="btn btn-warning btn-sm"
                           value="Modificar">
                </div>
            </form>
        </div>
    </div>
</div>