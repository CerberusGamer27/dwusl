<?php
require_once '../model/Categoria.php';
require_once '../model/DAOCategoria.php';  //nuevo

$respuesta= null;
$data = array();
$dao = new DAOCategoria();              //nuevo

if($_POST){
    if(isset($_POST["key"])){
        $key = $_POST["key"];
        $cat = new Categoria();
        switch ($key){
            case 'get':
                $respuesta = $dao->get();
                break;

            case 'fillData':
                $codigo = $_POST["codigo"];
                $respuesta = $dao->fillData($codigo);
                break;

            case 'insertar':
                //pendiente de programar
                parse_str($_POST["data"],$data);
                $cat->setNombreCat($data["txtNombre"]);
                $cat->setDescripcion($data["txtDescripcion"]);
                $respuesta = $dao->insertar($cat);
                break;

            case 'modificar':
                //pendiente de programar
                parse_str($_POST["data"],$data);
                $cat->setNombreCat($data["txtNombre"]);
                $cat->setDescripcion($data["txtDescripcion"]);
                $cat->setIdCategoria($data["txtCodigo"]);
                $respuesta = $dao->modificar($cat);
                break;

            case 'eliminar':
                $codigo = $_POST["codigo"];
                $respuesta= $dao->eliminar($codigo);
                break;
        }
    }
}
echo $respuesta;
?>

