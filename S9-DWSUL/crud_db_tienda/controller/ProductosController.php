<?php
require_once '../model/Productos.php';
require_once '../model/DAOProductos.php';  //nuevo

$respuesta= null;
$data = array();
$dao = new DAOProductos();              //nuevo

if($_POST){
    if(isset($_POST["key"])){
        $key = $_POST["key"];
        $prod = new Productos();
        switch ($key){
            case 'get':
                $respuesta = $dao->get();
                break;

            case 'getCategorias':
                $respuesta = $dao->getCategorias();
                break;

            case 'fillData':
                $codigo = $_POST["codigo"];
                $respuesta = $dao->fillData($codigo);
                break;

            case 'insertar':
                //pendiente de programar
                parse_str($_POST["data"],$data);
                $prod->setNombre($data["txtNombre"]);
                $prod->setStock($data["txtStock"]);
                $prod->setCosto($data["txtCosto"]);
                $prod->setIdcategoria($data["selectCategoria"]);
                $respuesta = $dao->insertar($prod);
                break;
            case 'modificar':
                parse_str($_POST["data"],$data);
                $prod->setCodigo($data["txtCodigo"]);
                $prod->setNombre($data["txtNombre"]);
                $prod->setStock($data["txtStock"]);
                $prod->setCosto($data["txtCosto"]);
                $prod->setIdcategoria($data["selectCategoria"]);
                $respuesta = $dao->modificar($prod);
                break;

            case 'eliminar':
                $codigo = $_POST["codigo"];
                $respuesta= $dao->eliminar($codigo);
                break;
        }
    }
}
echo $respuesta;
?>

