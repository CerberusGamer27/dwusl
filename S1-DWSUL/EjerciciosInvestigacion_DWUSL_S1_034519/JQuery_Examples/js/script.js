$.validator.setDefaults( {
    submitHandler: function () {
        alert( "submitted!" );
    }
} );


$( document ).ready(function() {
    console.log( "ready!" );
    $(".animsition").animsition({
        inClass: 'fade-in',
        outClass: 'fade-out',
        inDuration: 1500,
        outDuration: 800,
        linkElement: '.animsition-link',
        // e.g. linkElement: 'a:not([target="_blank"]):not([href^="#"])'
        loading: true,
        loadingParentElement: 'body', //animsition wrapper element
        loadingClass: 'animsition-loading',
        loadingInner: '', // e.g '<img src="loading.svg" />'
        timeout: false,
        timeoutCountdown: 5000,
        onLoadEvent: true,
        browser: [ 'animation-duration', '-webkit-animation-duration'],
        // "browser" option allows you to disable the "animsition" in case the css property in the array is not supported by your browser.
        // The default setting is to disable the "animsition" in a browser that does not support "animation-duration".
        overlay : false,
        overlayClass : 'animsition-overlay-slide',
        overlayParentElement : 'body',
        transition: function(url){ window.location.href = url; }
    });
    $( "#formExample" ).validate( {
        rules: {
            fullName: "required",
            username: {
                required: true,
                minlength: 5
            },
            email: {
                required: true,
                email: true
            },
            message: {
                required: true,
                minlength: 10
            }
        },
        messages: {
            fullName: "Ingrese su nombre completo",
            email: "Ingrese una direccion de email valida",
            message: "Ingrese un mensaje valido"
        },
        errorElement: "p",
        errorPlacement: function ( error, element ) {
            // Add the `invalid-feedback` class to the error element
            error.addClass('help');
            error.addClass('is-danger');
            element.closest('.control').append(error);
        }
        // },
        // highlight: function ( element, errorClass, validClass ) {
        //     $( element ).addClass( "is-invalid" ).removeClass( "is-valid" );
        // },
        // unhighlight: function (element, errorClass, validClass) {
        //     $( element ).addClass( "is-valid" ).removeClass( "is-invalid" );
        // }
    } );
});