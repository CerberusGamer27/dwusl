<?php
/*
Crear una vista en la cual se lleve
el control de cuantas visitas ha realizado
el cliente a este servidor, mediante
ip o nombre de host

- la vista tendra un formulario el cual guardará la
informacion en cookies para su posterior carga....

*/

//CONTADOR DE VISITAS
$ip = $_SERVER["REMOTE_ADDR"];
$nombreHost = gethostbyaddr($ip);
$cont = 1;
if (isset($_COOKIE["host"])) {
    $host = $_COOKIE["host"];
    if ($nombreHost == $host) {
        $cont = $_COOKIE["contador"];
        $cont++;
        setcookie("contador", $cont, time() + 3600);
    } else {
        setcookie("contador", 1, time() + 3600);
        setcookie("host", $nombreHost, time() + 3600);
    }
} else {
    setcookie("contador", 1, time() + 3600);
    setcookie("host", $nombreHost, time() + 3600);
}


?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css"
          integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.min.js"
            integrity="sha384-ODmDIVzN+pFdexxHEHFBQH3/9/vQ9uori45z4JjnFsRydbmQbmL5t1tQ0culUzyK"
            crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa"
            crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.6.1.js"
            integrity="sha256-3zlB5s2uwoUzrXK3BT7AX3FyvojsraNFxCc2vC/7pNI=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.4.29/dist/sweetalert2.all.min.js"
            integrity="sha256-qxQs7IPMvfbL9ZXhsZ3tG/LuZSjNxPSTBNtzk4j5FiU=" crossorigin="anonymous"></script>

    <script>
        $(document).ready(function () {

            <?php
            if(isset($_COOKIE["miCookie100"])){
            $nombre = $_COOKIE["nombre"];
            $edad = $_COOKIE["edad"];
            $opcion = $_COOKIE["opcion"];
            ?>
            Swal.fire({
                title: "Datos previos",
                text: "Desea cargar datos previos?",
                icon: "warning",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'SI'
            }).then((willDelete) => {
                if (willDelete.isConfirmed) {
                    <?php
                    $setNombre = "$(\"#txtNombre\").val('" . $nombre . "');";
                    $setEdad = "$(\"#txtEdad\").val('" . $edad . "');";
                    $setOpcion = "$(\"#txtOpcion option[value='" . $opcion . "']\").attr(\"selected\",true);";
                    echo $setNombre . $setEdad . $setOpcion;
                    }
                    ?>

                } else if (willDelete.dismiss) {
                    Swal.fire("No se cargaron datos previos...");
                }
            });
        });
    </script>


    <title>Document</title>
</head>
<body>
<div style="margin-left: auto; width: 50%;">
    Ip(<?= $ip ?>) | Host (<?= $nombreHost ?>) Visita # <?= $cont ?>
    <hr>
</div>

<form action="Cookies.php" method="post" class="small"
      style="width: 500px; margin: auto;">
    <input type="text" name="txtNombre" id="txtNombre" placeholder="Nombre completo"
           class="form-control">
    <input type="number" name="txtEdad" id="txtEdad" placeholder="Edad"
           class="form-control">
    <select name="txtOpcion" id="txtOpcion" class="form-control">
        <option value="1">valor 1</option>
        <option value="2">valor 2</option>
        <option value="3">valor 3</option>
    </select>
    <br>
    <input type="submit" name="btnEnviar" value="Procesar" class="btn btn-success sm">
</form>
<?php
if (isset($_POST["btnEnviar"])) {
    $nombre = $_POST["txtNombre"];
    $edad = $_POST["txtEdad"];
    $opcion = $_POST["txtOpcion"];


    //procedemos a guardar en cookies temporales
    setcookie("miCookie100", "exite", time() + 3600);
    setcookie("nombre", $nombre, time() + 3600);
    setcookie("edad", $edad, time() + 3600);
    setcookie("opcion", $opcion, time() + 3600);

    //procesar como quiera los datos del form.....

}


?>


</body>
</html>
