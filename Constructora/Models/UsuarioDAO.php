<?php
require_once ('config.php');
class UsuarioDAO
{
    private function conectar(){
        try {
            return new PDO(DSN,USER,PASSWORD);
        }catch (Exception $error){
            return $error;
        }
    }

    public function logIn($username, $password){
        try {
            $conexion = $this->conectar();
            $sql= "SELECT * FROM usuario WHERE username=? AND password=?";
            return json_encode($conexion->prepare($sql)->execute([$username,strtoupper(md5($password))]));
        }catch (Exception $error){
            return json_encode($error->getMessage());
        } finally {
            $conexion = null;
        }
    }
}