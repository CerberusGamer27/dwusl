<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: *");
header("Access-Control-Allow-Headers: *");
header('Content-type: application/json');

require_once('../Models/UsuarioDAO.php');

$usuarioDAO = new UsuarioDAO();

switch($_SERVER['REQUEST_METHOD']){
    case 'POST':
        if(isset($_POST['method'])){
            $method=$_POST['method'];
            switch ($method){
                case 'logIn':
                    echo $usuarioDAO->logIn($_POST['username'],$_POST['password']);
                    break;
            }
        }
        break;

    case 'GET':
        break;

}